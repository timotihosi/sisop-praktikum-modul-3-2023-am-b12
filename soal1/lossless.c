#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>
#include <ctype.h>

#define MAX_TREE_HT 50

struct MinHNode
{
    char item;
    unsigned freq;
    struct MinHNode *left, *right;
};

struct MinHeap
{
    unsigned size;
    unsigned capacity;
    struct MinHNode **array;
};

struct MinHNode *newNode(char item, unsigned freq)
{
    struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

    temp->left = temp->right = NULL;
    temp->item = item;
    temp->freq = freq;

    return temp;
}

int isLeaf(struct MinHNode *root)
{
    return !(root->left) && !(root->right);
}

char letDec(struct MinHNode *node, int *index, char *str, char res)
{
    while (node->left != NULL || node->right != NULL)
    {
        if (str[*index] == '0')
        {
            node = node->left;
        }
        else
        {
            node = node->right;
        }
        (*index)++;
    }
    return node->item;
}

void textDec(char *str, struct MinHNode *node, char *text)
{
    int index = 0;
    while (index < strlen(str))
    {
        struct MinHNode *current = node;
        while (current->left != NULL || current->right != NULL)
        {
            if (str[index] == '0')
            {
                current = current->left;
            }
            else
            {
                current = current->right;
            }
            index++;
        }
        char resString[2];
        resString[0] = current->item;
        resString[1] = '\0';
        strcat(text, resString);
    }
}

void textEnc(char *str, char **huffmanCode)
{
    FILE *fp;
    char fileName[] = "file.txt";
    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        printf("Gagal Membuka File\n");
        return;
    }

    int c;
    while ((c = fgetc(fp)) != EOF)
    {
        if (isalpha(c))
        {
            c = toupper(c);
            strcat(str, huffmanCode[c - 'A']);
        }
    }

    fclose(fp);
}

void getSBT(struct MinHNode *node, char *str, int *index, int isRight)
{
    if (node == NULL)
    {
        return;
    }

    str[(*index)++] = node->item;

    if (node->left == NULL && node->right == NULL)
    {
        return;
    }

    str[(*index)++] = '(';
    getSBT(node->left, str, index, 1);
    str[(*index)++] = ')';

    if (node->right != NULL)
    {
        str[(*index)++] = '(';
        getSBT(node->right, str, index, 1);
        str[(*index)++] = ')';
    }
}

struct MinHNode *stringToTree(char *str, int *index)
{
    if (str[*index] == ')')
    {
        return NULL;
    }

    struct MinHNode *node = newNode(str[*index], 0);

    (*index)++;

    if (str[*index] != '(')
    {
        (*index)++;
        return node;
    }

    (*index)++;
    node->left = stringToTree(str, index);

    (*index)++;
    node->right = stringToTree(str, index);

    (*index)++;

    return node;
}

void getHuffCodes(struct MinHNode *root, int arr[], int top, char **huffmanCode)
{
    if (root->left)
    {
        arr[top] = 0;
        getHuffCodes(root->left, arr, top + 1, huffmanCode);
    }
    if (root->right)
    {
        arr[top] = 1;
        getHuffCodes(root->right, arr, top + 1, huffmanCode);
    }
    if (isLeaf(root))
    {
        char *binary = (char *)malloc((top + 1) * sizeof(char));
        for (int i = 0; i < top; i++)
        {
            binary[i] = arr[i] + '0';
        }
        binary[top] = '\0';
        int idx = root->item - 65;
        printf("  %d %c   |  %s\n", idx, root->item, binary);
        huffmanCode[idx] = binary;
    }
}

void inorderTraversal(struct MinHNode *root)
{
    if (root == NULL)
    {
        return;
    }
    inorderTraversal(root->left);
    printf("%c ", root->item);
    inorderTraversal(root->right);
}

struct MinHeap *createMinH(unsigned capacity)
{
    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

    minHeap->size = 0;

    minHeap->capacity = capacity;

    minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
    return minHeap;
}

void swapMinHNode(struct MinHNode **a, struct MinHNode **b)
{
    struct MinHNode *t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx)
{
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx)
    {
        swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

int checkSizeOne(struct MinHeap *minHeap)
{
    return (minHeap->size == 1);
}

struct MinHNode *extractMin(struct MinHeap *minHeap)
{
    struct MinHNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode)
{
    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq)
    {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap)
{
    int n = minHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}

struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size)
{
    struct MinHeap *minHeap = createMinH(size);

    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(item[i], freq[i]);

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size)
{
    struct MinHNode *left, *right, *top;
    struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

    while (!checkSizeOne(minHeap))
    {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}

char *getBinaryCode(int arr[], int n)
{
    char *res = (char *)malloc(sizeof(char) * (n + 1));
    int i;
    for (i = 0; i < n; ++i)
    {
        res[i] = arr[i] + '0';
    }

    return res;
}

void printArray(int arr[], int n)
{
    int i;
    for (i = 0; i < n; ++i)
        printf("%d", arr[i]);

    printf("\n");
}

void printHCodes(struct MinHNode *root, int arr[], int top)
{
    if (root->left)
    {
        arr[top] = 0;
        printHCodes(root->left, arr, top + 1);
    }
    if (root->right)
    {
        arr[top] = 1;
        printHCodes(root->right, arr, top + 1);
    }
    if (isLeaf(root))
    {
        printf("  %c   | ", root->item);
        printArray(arr, top);
    }
}

struct MinHNode HuffCodes(char item[], int freq[], int size)
{
    struct MinHNode *root = buildHuffmanTree(item, freq, size);

    int arr[MAX_TREE_HT], top = 0;

    return *root;
}

int main()
{
    FILE *fp;
    char fileName[] = "file.txt";
    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        printf("Gagal membuka file\n");
        return 1;
    }

    int freq[26] = {0};
    int c;
    int letterCount = 0;
    while ((c = fgetc(fp)) != EOF)
    {
        if (isalpha(c))
        {
            letterCount++;
            c = toupper(c);
            freq[c - 'A']++;
        }
    }

    fclose(fp);

    char letters[30];
    int fq[30];

    int currIdx = 0;
    for (int i = 0; i < 26; i++)
    {
        if (freq[i] > 0)
        {
            letters[currIdx] = 'A' + i;
            fq[currIdx] = freq[i];
            currIdx++;
        }
    }

    int fd1[2]; 
    int fd2[2]; 

    if (pipe(fd1) == -1 || pipe(fd2) == -1)
    {
        perror("pipe");
        return 1;
    }

    pid_t c_pid = fork();

    if (c_pid < 0)
    {
        fprintf(stderr, "Gagal fork");
        return 1;
    }

    if (c_pid > 0)
    {
        wait(NULL);
        printf("\n");
        printf("Parent Proccess\n");
        close(fd1[1]);
        close(fd2[1]);

        char *stringTree = (char *)malloc(sizeof(char) * 150);
        char *encodedText = (char *)malloc(sizeof(char) * 3000);

        read(fd1[0], stringTree, 150);
        read(fd2[0], encodedText, 3000);

        int idxTree = 0;
        struct MinHNode *root = stringToTree(stringTree, &idxTree);

        char **huffmanCode = (char **)malloc(sizeof(char) * 10 * 30);

        char *text = (char *)malloc(sizeof(char) * 1000);
        textDec(encodedText, root, text);
        printf("%s\n", text);
        printf("\n");

        printf("Membandingkan ...\n");
        int originalFileSizeBits = letterCount * 8;
        int compressedFileSizeBits = strlen(text);

        printf("File asli  : %d \n", originalFileSizeBits);
        printf("Teks terkompres  : %d \n", compressedFileSizeBits);
    }

    if (c_pid == 0)
    {
        close(fd2[0]);
        close(fd1[0]);

        printf("Membuat huffman tree ... \n");
        struct MinHNode tree = HuffCodes(letters, fq, currIdx);
        struct MinHNode *root = &tree;
        char *str = (char *)malloc(sizeof(char) * 150);
        int index = 0;
        getSBT(root, str, &index, 0);
        str[index] = '\0';
        printf("%s\n", str);
        write(fd1[1], str, strlen(str) + 1);

        printf("Membuat huffman code ... \n");
        char **huffmanCode = (char **)malloc(sizeof(char) * 10 * 30);

        int arr[MAX_TREE_HT], top = 0;
        getHuffCodes(root, arr, top, huffmanCode);

        for (int i = 0; i < 22; i++)

            printf("Mengencode teks ... \n");
        char *encodedText = (char *)malloc(sizeof(char) * 3000);
        textEnc(encodedText, huffmanCode);
        printf("%s\n", encodedText);

        write(fd2[1], encodedText, strlen(encodedText) + 1);
    }
}
