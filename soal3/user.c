#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/sem.h>

#define MAX_MSG_SIZE 100

struct msgbuf {
    long mtype;
    char mtext[MAX_MSG_SIZE];
};

int main() {
	
	// Create message queue and semaphore
	key_t stream_key = ftok("stream.c", 'M');
	if(stream_key == -1){
		perror("ftok failed");
	    exit(1);	
	}
	
	int stream_queue = msgget(stream_key, IPC_CREAT | 0666);
	if (stream_queue == -1){
		perror("msgget failed");
	    exit(1);
	}
	
	key_t sem_key = ftok("user.c", 'S');
	if(sem_key == -1){
		perror("ftok failed");
	    exit(1);	
	}
	
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}
	
	union semun {
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	};
	

	union semun arg;
	struct sembuf sb = {0, -1, 0}; // Wait operation
	
	arg.val = 2;
	
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl");
    	exit(1);
	}

	// Send messages to stream.c
  	char message[MAX_MSG_SIZE];
  	
  	pid_t p = getpid();
	printf("User %d connected to stream.\n", p);

  	
	while (1) {
	    // Wait for semaphore to become available
	    struct sembuf sem_wait = {0, -1, 0};
	    semop(sem_id, &sem_wait, 1);

	    printf("Enter message: ");
	    fgets(message, MAX_MSG_SIZE, stdin);
	    message[strcspn(message, "\n")] = '\0';

	    // Send message to stream.c
	    struct msgbuf msg;
	    msg.mtype = p;
	    
	    if(!strcmp(message, "DECRYPT")){
	    	strncpy(msg.mtext, message, MAX_MSG_SIZE);
		}
	    
    	else if(!strcmp(message, "LIST")){
	    	strncpy(msg.mtext, message, MAX_MSG_SIZE);
		}
		
		else if(!strncmp(message, "PLAY", 4)){
			
			char *as = strtok(message + 5, "\"");
		    if (as == NULL){
		        printf("Invalid Parameter: %s\n", message);
		        continue;
		    }
		    // Convert song to uppercase for case-insensitive comparison
		    for (int i = 0; i < strlen(as); i++){
		        as[i] = toupper(as[i]);
		    }

	    	sprintf(msg.mtext, "PLAY %s", as);
		}
		
		else if(!strncmp(message, "ADD", 3)){
			
			char *as = strtok(message + 4, "\n");
		    if (as == NULL){
		        printf("Invalid Parameter: %s\n", message);
		        continue;
		    }
	    	sprintf(msg.mtext, "ADD %s", as);
		}
		
		else{
			printf("Invalid Command: %s\n", message);
		    continue;
		}
    
   		if(msgsnd(stream_queue, &msg, MAX_MSG_SIZE, 0) == -1){
   			perror("msgsnd failed");
	    	exit(1);	
		}

	    // Release semaphore
	    struct sembuf sem_signal = {0, 1, 0};
	    semop(sem_id, &sem_signal, 1);
  }

  // Remove message queue and semaphore
  msgctl(stream_queue, IPC_RMID, NULL);
  semctl(sem_id, 0, IPC_RMID, arg);

  return 0;
}
