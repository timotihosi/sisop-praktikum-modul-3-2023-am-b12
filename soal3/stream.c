#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <cjson/cJSON.h>
#include <sys/msg.h>
#include <errno.h>
#include <sys/sem.h>

#define MAX_MSG_SIZE 100
#define MAX_USERS 2

//  gcc -o stream stream.c -lcjson

struct msgbuf
{
  long mtype;
  char mtext[MAX_MSG_SIZE];
};

static char* rot13(const char* input)
{
    const int len = strlen(input);
    char* output = (char*)malloc(len + 1);
    if (output == NULL) {
        return NULL;
    }

    for (int i = 0; i < len; i++) {
        char c = input[i];
        if (isalpha(c)) {
            char base = islower(c) ? 'a' : 'A';
            c = base + (c - base + 13) % 26;
        }
        output[i] = c;
    }
    output[len] = '\0';
    return output;
}

//static const unsigned char base64_table[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/'};
static const unsigned char base64_table[256] = {['A'] = 0, ['B'] = 1, ['C'] = 2, ['D'] = 3, ['E'] = 4, ['F'] = 5, ['G'] = 6, ['H'] = 7, ['I'] = 8, ['J'] = 9, ['K'] = 10, ['L'] = 11, ['M'] = 12, ['N'] = 13, ['O'] = 14, ['P'] = 15, ['Q'] = 16, ['R'] = 17, ['S'] = 18, ['T'] = 19, ['U'] = 20, ['V'] = 21, ['W'] = 22, ['X'] = 23, ['Y'] = 24, ['Z'] = 25, ['a'] = 26, ['b'] = 27, ['c'] = 28, ['d'] = 29, ['e'] = 30, ['f'] = 31, ['g'] = 32, ['h'] = 33, ['i'] = 34, ['j'] = 35, ['k'] = 36, ['l'] = 37, ['m'] = 38, ['n'] = 39, ['o'] = 40, ['p'] = 41, ['q'] = 42, ['r'] = 43, ['s'] = 44, ['t'] = 45, ['u'] = 46, ['v'] = 47, ['w'] = 48, ['x'] = 49, ['y'] = 50, ['z'] = 51, ['0'] = 52, ['1'] = 53, ['2'] = 54, ['3'] = 55, ['4'] = 56, ['5'] = 57, ['6'] = 58, ['7'] = 59, ['8'] = 60, ['9'] = 61, ['+'] = 62, ['/'] = 63};

// Base64 decoding function
static char *base64(const char *input)
{
  int len = strlen(input);
  if (len % 4 != 0)
  {
    printf("Input length is not a multiple of 4\n");
    return NULL;
  }

  int output_len = 3 * (len / 4);
  if (input[len - 1] == '=')
    output_len--;
  if (input[len - 2] == '=')
    output_len--;

  char *output = malloc(output_len + 1);
  if (output == NULL)
  {
    printf("Failed to allocate memory for output\n");
    return NULL;
  }

  char *output_ptr = output;
  for (int i = 0; i < len; i += 4)
  {
    unsigned char values[4] = {
        base64_table[(unsigned char)input[i]],
        base64_table[(unsigned char)input[i + 1]],
        base64_table[(unsigned char)input[i + 2]],
        base64_table[(unsigned char)input[i + 3]]};

    *output_ptr++ = (values[0] << 2) | (values[1] >> 4);
    if (input[i + 2] != '=')
      *output_ptr++ = (values[1] << 4) | (values[2] >> 2);
    if (input[i + 3] != '=')
      *output_ptr++ = (values[2] << 6) | values[3];
  }

  *output_ptr = '\0';
  return output;
}

static char *hex(const char *input)
{
  int len = strlen(input);
  if (len % 2 != 0)
  {
    return NULL;
  }

  int output_len = len / 2;
  char *output = malloc(output_len + 1);
  if (output == NULL)
  {
    return NULL;
  }

  char *output_ptr = output;
  for (int i = 0; i < len; i += 2)
  {
    char hex[3] = {input[i], input[i + 1], '\0'};
    int value = strtol(hex, NULL, 16);
    *output_ptr++ = (char)value;
  }

  *output_ptr = '\0';
  return output;
}

void add_song(char *song_name){
    // Open the playlist file for reading
    FILE *playlist = fopen("playlist.txt", "r");
    if (playlist == NULL)
    {
        printf("Error opening playlist file for reading\n");
        return;
    }

    // Check if song is already in the playlist
    char line[100];
    while (fgets(line, 100, playlist) != NULL)
    {
        line[strcspn(line, "\n")] = '\0'; // Remove newline character
        if (strcasecmp(line, song_name) == 0)
        {
            printf("SONG ALREADY ON PLAYLIST\n");
            fclose(playlist);
            return;
        }
    }

    // Close the file
    fclose(playlist);

    // Open the playlist file for appending
    playlist = fopen("playlist.txt", "a");
    if (playlist == NULL)
    {
        printf("Error opening playlist file for appending\n");
        return;
    }

    // Write song to the playlist
    fprintf(playlist, "%s\n", song_name);
    printf("USER <%d> ADD %s\n", getpid(), song_name);

    // Close the file
    fclose(playlist);
}


int main(){

	// Create message queue and semaphore
	key_t stream_key = ftok("stream.c", 'M');
	int stream_queue = msgget(stream_key, IPC_CREAT | 0666);
	if (stream_queue == -1){
		perror("msgget failed");
	    exit(1);
	}

	key_t sem_key = ftok("user.c", 'S');
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}

  // Initialize semaphore to 2 (maximum number of users)
  	union semun{
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	} arg;
  
  	arg.val = MAX_USERS;
	  
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl failed");
   		exit(1);
	}
	
	while (1){
		
	    struct msgbuf msg;
	    int len = msgrcv(stream_queue, &msg, MAX_MSG_SIZE, 0, 0);
	    if (len == -1){
	    	perror("msgrcv failed");
	    	exit(1);
	    }
	
	    // Wait for semaphore
	    struct sembuf sb = {0, -1, 0};
	    if (semop(sem_id, &sb, 1) == -1)
	    {
			perror("semop failed");
			exit(1);
	    }
	    
	    // Parse user command
	    char *command = msg.mtext;
	    char *param = strchr(command, ' ');
	    if (param){
	    	*param = '\0';
	    	param++;
	    	while (isspace(*param)){
        		param++;
      		}
    	}

		//DECRYPT	
		if (!strcmp(command, "DECRYPT")){
			FILE *fp = fopen("song-playlist.json", "r");
			if (fp == NULL){
        		printf("Error opening file: %s\n", "song-playlist.json");
        		return 1;
      		}
      		
      		fseek(fp, 0, SEEK_END);
		    long file_size = ftell(fp);
		    fseek(fp, 0, SEEK_SET);
		    
      		char *json_str = malloc(file_size + 1);
		    if (json_str == NULL){
		        printf("Error allocating memory\n");
		        return 1;
		    }
	      	fread(json_str, 1, file_size, fp);
	      	fclose(fp);
	      	json_str[file_size] = '\0';

	      	cJSON *root = cJSON_Parse(json_str);
	      	if (root == NULL){
	        	printf("Error parsing JSON: %s\n", cJSON_GetErrorPtr());
	        	free(json_str);
	        	return 1;
	      	}

      		FILE *playlist = fopen("playlist.txt", "w");
      		if (playlist == NULL){
		        printf("Error opening playlist file for writing\n");
		        cJSON_Delete(root);
		        free(json_str);
		        return 1;
      		}

		    cJSON *item = NULL;
		    cJSON_ArrayForEach(item, root){
		        cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
		        cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");
				
        		if (strcmp(method->valuestring, "base64") == 0){
			        char *Dcd = base64(song->valuestring);
			        if (Dcd == NULL){
			        	printf("Error decoding base64: %s\n", song->valuestring);
			            continue;
			        }
			        printf("Base64: %s\n", Dcd);
			        fprintf(playlist, "%s\n", Dcd);
			        free(Dcd);
		        }
		        
		        else if (strcmp(method->valuestring, "hex") == 0){
		        	char *Dcd = hex(song->valuestring);
		        	if (Dcd == NULL){
		            	printf("Error decoding hex: %s\n", song->valuestring);
		            	continue;
		          	}
		        	printf("Hex: %s\n", Dcd);
		        	fprintf(playlist, "%s\n", Dcd);
		        	free(Dcd);
		        }
		        
        		else if (strcmp(method->valuestring, "rot13") == 0){
			        char *Dcd = rot13(song->valuestring);
			        if (Dcd == NULL){
			            printf("Error decoding rot13: %s\n", song->valuestring);
			            continue;
			        }
			        printf("Rot13: %s\n", Dcd);
			        fprintf(playlist, "%s\n", Dcd);
			        free(Dcd);
		        }
		        
        		else{
		        	printf("Unknown method: %s\n", method->valuestring);
		        	continue;
		        }
      		}

	    	fclose(playlist);
	    	cJSON_Delete(root);
	    	free(json_str);
    	}
    	
    	//LIST
		else if (!strcmp(command, "LIST")){
			FILE *pl = fopen("playlist.txt", "r");
			if (pl == NULL){
				printf("Error opening playlist file for reading\n");
				exit(1);
			}
			 fclose(pl);
			 system("sort playlist.txt");
  		}

		//PLAY
		else if (!strcmp(command, "PLAY")){
			
			char *query = param;
		    int num_matches = 0;
		    char matches[1000][100];
			char songnm[100];
			
		    FILE *pl = fopen("playlist.txt", "r");
		    
		    if (pl == NULL){
		        printf("Error opening playlist file for reading\n");
		        exit(1);
		    }

		    while (fgets(songnm, 100, pl) != NULL)
		    {
		        // Remove trailing newline if present
		        songnm[strcspn(songnm, "\n")] = '\0';
		
		        // Check if song name contains the query
		        char *found = strcasestr(songnm, query);
		        if (found != NULL)
		        {
		            strcpy(matches[num_matches], songnm);
		            num_matches+=1;
		        }
		        
		    }
		
		    fclose(pl);
		
		    if (num_matches == 0){
		        printf("THERE IS NO SONG CONTAINING \"%s\"\n", query);
		    }
		    
		    else if (num_matches == 1){
		         printf("USER <%d> PLAYING \"%s\"\n", getpid(), matches[0]);
		    }
		    
		    else{
		        printf("THERE ARE \"%d\" SONG(S) CONTAINING \"%s\":\n", num_matches, query);
		        for (int i = 0; i < num_matches; i++){
		            printf("%d. %s\n", i + 1, matches[i]);
		        }
		    }
  		}
  		
  		//ADD
		else if (!strcmp(command, "ADD")){
			add_song(param);
		}

	}	

	return 0;
}







