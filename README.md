# Praktikum Sistem Operasi Modul 3 Kelompok B12

## Group Member    :
| Nama                              | NRP        |
|-----------------------------------|------------|
|Timothy Hosia Budianto             |5025211098  |
|Arif Nugraha Santosa               |5025211048  |
|Abdullah Yasykur Bifadhlil Midror  |5025211035  |

Berikut adalah laporan kami mengenai praktikum modul 3

## Nomor 1
### Soal
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan **Algoritma Huffman** untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah <a href="https://drive.google.com/file/d/1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C/view" target="_blank">file</a>. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 

**(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).**

- Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
- Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
- Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
- Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
- Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

### Jawab

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>
#include <ctype.h>

#define MAX_TREE_HT 50

struct MinHNode
{
    char item;
    unsigned freq;
    struct MinHNode *left, *right;
};

struct MinHeap
{
    unsigned size;
    unsigned capacity;
    struct MinHNode **array;
};

struct MinHNode *newNode(char item, unsigned freq)
{
    struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

    temp->left = temp->right = NULL;
    temp->item = item;
    temp->freq = freq;

    return temp;
}

int isLeaf(struct MinHNode *root)
{
    return !(root->left) && !(root->right);
}

char letDec(struct MinHNode *node, int *index, char *str, char res)
{
    while (node->left != NULL || node->right != NULL)
    {
        if (str[*index] == '0')
        {
            node = node->left;
        }
        else
        {
            node = node->right;
        }
        (*index)++;
    }
    return node->item;
}

void textDec(char *str, struct MinHNode *node, char *text)
{
    int index = 0;
    while (index < strlen(str))
    {
        struct MinHNode *current = node;
        while (current->left != NULL || current->right != NULL)
        {
            if (str[index] == '0')
            {
                current = current->left;
            }
            else
            {
                current = current->right;
            }
            index++;
        }
        char resString[2];
        resString[0] = current->item;
        resString[1] = '\0';
        strcat(text, resString);
    }
}

void textEnc(char *str, char **huffmanCode)
{
    FILE *fp;
    char fileName[] = "file.txt";
    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        printf("Gagal Membuka File\n");
        return;
    }

    int c;
    while ((c = fgetc(fp)) != EOF)
    {
        if (isalpha(c))
        {
            c = toupper(c);
            strcat(str, huffmanCode[c - 'A']);
        }
    }

    fclose(fp);
}

void getSBT(struct MinHNode *node, char *str, int *index, int isRight)
{
    if (node == NULL)
    {
        return;
    }

    str[(*index)++] = node->item;

    if (node->left == NULL && node->right == NULL)
    {
        return;
    }

    str[(*index)++] = '(';
    getSBT(node->left, str, index, 1);
    str[(*index)++] = ')';

    if (node->right != NULL)
    {
        str[(*index)++] = '(';
        getSBT(node->right, str, index, 1);
        str[(*index)++] = ')';
    }
}

struct MinHNode *stringToTree(char *str, int *index)
{
    if (str[*index] == ')')
    {
        return NULL;
    }

    struct MinHNode *node = newNode(str[*index], 0);

    (*index)++;

    if (str[*index] != '(')
    {
        (*index)++;
        return node;
    }

    (*index)++;
    node->left = stringToTree(str, index);

    (*index)++;
    node->right = stringToTree(str, index);

    (*index)++;

    return node;
}

void getHuffCodes(struct MinHNode *root, int arr[], int top, char **huffmanCode)
{
    if (root->left)
    {
        arr[top] = 0;
        getHuffCodes(root->left, arr, top + 1, huffmanCode);
    }
    if (root->right)
    {
        arr[top] = 1;
        getHuffCodes(root->right, arr, top + 1, huffmanCode);
    }
    if (isLeaf(root))
    {
        char *binary = (char *)malloc((top + 1) * sizeof(char));
        for (int i = 0; i < top; i++)
        {
            binary[i] = arr[i] + '0';
        }
        binary[top] = '\0';
        int idx = root->item - 65;
        printf("  %d %c   |  %s\n", idx, root->item, binary);
        huffmanCode[idx] = binary;
    }
}

void inorderTraversal(struct MinHNode *root)
{
    if (root == NULL)
    {
        return;
    }
    inorderTraversal(root->left);
    printf("%c ", root->item);
    inorderTraversal(root->right);
}

struct MinHeap *createMinH(unsigned capacity)
{
    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

    minHeap->size = 0;

    minHeap->capacity = capacity;

    minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
    return minHeap;
}

void swapMinHNode(struct MinHNode **a, struct MinHNode **b)
{
    struct MinHNode *t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx)
{
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx)
    {
        swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

int checkSizeOne(struct MinHeap *minHeap)
{
    return (minHeap->size == 1);
}

struct MinHNode *extractMin(struct MinHeap *minHeap)
{
    struct MinHNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode)
{
    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq)
    {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap)
{
    int n = minHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}

struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size)
{
    struct MinHeap *minHeap = createMinH(size);

    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(item[i], freq[i]);

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size)
{
    struct MinHNode *left, *right, *top;
    struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

    while (!checkSizeOne(minHeap))
    {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}

char *getBinaryCode(int arr[], int n)
{
    char *res = (char *)malloc(sizeof(char) * (n + 1));
    int i;
    for (i = 0; i < n; ++i)
    {
        res[i] = arr[i] + '0';
    }

    return res;
}

void printArray(int arr[], int n)
{
    int i;
    for (i = 0; i < n; ++i)
        printf("%d", arr[i]);

    printf("\n");
}

void printHCodes(struct MinHNode *root, int arr[], int top)
{
    if (root->left)
    {
        arr[top] = 0;
        printHCodes(root->left, arr, top + 1);
    }
    if (root->right)
    {
        arr[top] = 1;
        printHCodes(root->right, arr, top + 1);
    }
    if (isLeaf(root))
    {
        printf("  %c   | ", root->item);
        printArray(arr, top);
    }
}

struct MinHNode HuffCodes(char item[], int freq[], int size)
{
    struct MinHNode *root = buildHuffmanTree(item, freq, size);

    int arr[MAX_TREE_HT], top = 0;

    return *root;
}

int main()
{
    FILE *fp;
    char fileName[] = "file.txt";
    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        printf("Gagal membuka file\n");
        return 1;
    }

    int freq[26] = {0};
    int c;
    int letterCount = 0;
    while ((c = fgetc(fp)) != EOF)
    {
        if (isalpha(c))
        {
            letterCount++;
            c = toupper(c);
            freq[c - 'A']++;
        }
    }

    fclose(fp);

    char letters[30];
    int fq[30];

    int currIdx = 0;
    for (int i = 0; i < 26; i++)
    {
        if (freq[i] > 0)
        {
            letters[currIdx] = 'A' + i;
            fq[currIdx] = freq[i];
            currIdx++;
        }
    }

    int fd1[2]; 
    int fd2[2]; 

    if (pipe(fd1) == -1 || pipe(fd2) == -1)
    {
        perror("pipe");
        return 1;
    }

    pid_t c_pid = fork();

    if (c_pid < 0)
    {
        fprintf(stderr, "Gagal fork");
        return 1;
    }

    if (c_pid > 0)
    {
        wait(NULL);
        printf("\n");
        printf("Parent Proccess\n");
        close(fd1[1]);
        close(fd2[1]);

        char *stringTree = (char *)malloc(sizeof(char) * 150);
        char *encodedText = (char *)malloc(sizeof(char) * 3000);

        read(fd1[0], stringTree, 150);
        read(fd2[0], encodedText, 3000);

        int idxTree = 0;
        struct MinHNode *root = stringToTree(stringTree, &idxTree);

        char **huffmanCode = (char **)malloc(sizeof(char) * 10 * 30);

        char *text = (char *)malloc(sizeof(char) * 1000);
        textDec(encodedText, root, text);
        printf("%s\n", text);
        printf("\n");

        printf("Membandingkan ...\n");
        int originalFileSizeBits = letterCount * 8;
        int compressedFileSizeBits = strlen(text);

        printf("File asli  : %d \n", originalFileSizeBits);
        printf("Teks terkompres  : %d \n", compressedFileSizeBits);
    }

    if (c_pid == 0)
    {
        close(fd2[0]);
        close(fd1[0]);

        printf("Membuat huffman tree ... \n");
        struct MinHNode tree = HuffCodes(letters, fq, currIdx);
        struct MinHNode *root = &tree;
        char *str = (char *)malloc(sizeof(char) * 150);
        int index = 0;
        getSBT(root, str, &index, 0);
        str[index] = '\0';
        printf("%s\n", str);
        write(fd1[1], str, strlen(str) + 1);

        printf("Membuat huffman code ... \n");
        char **huffmanCode = (char **)malloc(sizeof(char) * 10 * 30);

        int arr[MAX_TREE_HT], top = 0;
        getHuffCodes(root, arr, top, huffmanCode);

        for (int i = 0; i < 22; i++)

            printf("Mengencode teks ... \n");
        char *encodedText = (char *)malloc(sizeof(char) * 3000);
        textEnc(encodedText, huffmanCode);
        printf("%s\n", encodedText);

        write(fd2[1], encodedText, strlen(encodedText) + 1);
    }
}

```

### Penjelasan
1. Library dan Konstanta:
    - Kode dimulai dengan beberapa direktif preprosesor dan deklarasi library yang diperlukan.
    - Konstanta MAX_TREE_HT digunakan untuk mengatur ukuran maksimum pohon Huffman.

2. Struktur Data:
    - Terdapat dua struktur data yang didefinisikan: MinHNode dan MinHeap.
    - MinHNode merepresentasikan simpul dalam pohon Huffman. It memiliki karakter item, frekuensi freq, dan pointer ke simpul anak kiri dan kanan.
    - MinHeap merepresentasikan min heap, yang digunakan dalam pembangunan pohon Huffman. It memiliki ukuran, kapasitas, dan array yang berisi pointer ke simpul-simpul MinHNode.

3. Fungsi Utilitas:
    - newNode digunakan untuk membuat simpul baru dengan karakter dan frekuensi yang diberikan.
    - isLeaf digunakan untuk memeriksa apakah suatu simpul merupakan simpul daun (tidak memiliki anak).
    - letDec digunakan untuk melakukan dekompresi pada suatu teks biner dan mengembalikan karakter yang sesuai berdasarkan pohon Huffman.
    - textDec digunakan untuk mendekompresi seluruh teks biner menjadi teks asli menggunakan pohon Huffman.
    - textEnc digunakan untuk mengompresi teks dari file "file.txt" menggunakan kode Huffman.
    - getSBT digunakan untuk mengambil representasi string dari pohon Huffman.
    - stringToTree digunakan untuk mengkonversi string yang mewakili pohon Huffman menjadi pohon Huffman yang sesungguhnya.
    - getHuffCodes digunakan untuk mendapatkan kode Huffman untuk setiap karakter dalam pohon Huffman.
    - inorderTraversal digunakan untuk melakukan traversal inorder pada pohon Huffman.
    - createMinH digunakan untuk membuat min heap dengan kapasitas yang diberikan.
    - swapMinHNode digunakan untuk menukar dua simpul dalam min heap.
    - minHeapify digunakan untuk menjaga sifat heap pada min heap.
    - checkSizeOne digunakan untuk memeriksa apakah ukuran min heap adalah satu.
    - extractMin digunakan untuk menghapus dan mengembalikan simpul dengan frekuensi terendah dari min heap.
    - insertMinHeap digunakan untuk menyisipkan simpul ke dalam min heap.
    - buildMinHeap digunakan untuk membangun min heap dari array yang diberikan.
    - createAndBuildMinHeap digunakan untuk membuat dan membangun min heap berdasarkan karakter dan frekuensi yang diberikan.
    - buildHuffmanTree digunakan untuk membangun pohon Huffman berdasarkan karakter dan frekuensi yang diberikan.
    - getBinaryCode digunakan untuk mendapatkan representasi biner dari array bilangan bulat.
    - printArray digunakan untuk mencetak array bilangan bulat.
    - printHCodes digunakan untuk mencetak kode Huffman untuk setiap karakter dalam pohon Huffman.
    - HuffCodes digunakan untuk membangun pohon Huffman dan mengembalikan simpul akar pohon Huffman yang telah dibangun.

4. Fungsi main:
    - Fungsi main merupakan titik masuk utama program.
    - ertama, program membuka file "file.txt" untuk membaca teks yang akan dikompresi.
    - Selanjutnya, program menghitung frekuensi kemunculan setiap karakter dalam teks dan menyimpannya dalam array freq.
    - Kemudian, program melakukan fork untuk membuat proses anak.
    - Pada proses anak (c_pid == 0), program melakukan kompresi teks menggunakan algoritma Huffman.
     Pertama, program membangun pohon Huffman dengan memanggil fungsi HuffCodes dan menyimpan akar pohon Huffman.
     Selanjutnya, program mengonversi pohon Huffman menjadi representasi string dan mengirimnya melalui pipe ke proses induk.
     Program juga menghasilkan kode Huffman untuk setiap karakter dan mengompresi teks menggunakan kode Huffman tersebut. Teks  hasil   kompresi dikirim melalui pipe ke proses induk.
    - Pada proses induk (c_pid > 0), program menerima representasi string pohon Huffman dan teks hasil kompresi dari proses anak melalui pipe.
    - Program mengonversi representasi string pohon Huffman menjadi pohon Huffman yang sesungguhnya menggunakan fungsi stringToTree.
     Selanjutnya, program melakukan dekompresi teks hasil kompresi menggunakan pohon Huffman.
     Program juga membandingkan ukuran file asli (dalam bit) dengan ukuran teks hasil kompresi untuk melihat efisiensi kompresi yang dicapai.

5. Output:
    Program mencetak teks hasil dekompresi, representasi pohon Huffman, dan perbandingan ukuran file asli dengan ukuran teks hasil kompresi.

**Output**
![output1.png](./images/output1.jpeg)
![output2.png](./images/output2.jpeg)

**Kesulitan Nomor 1 :**
Implementasi algoritma huffman tree

## Nomor 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

**Penerapan:**

#### Soal A

Membuat program C dengan nama `kalian.c`, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

**Penyelesaian:**
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int main() {
    int shmid;
    key_t key = 1234;
    int *matrix1, *matrix2, *matrix3;
    srand(time(NULL));

    // allocate shared memory for matrices
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segments to matrices
    matrix1 = shmat(shmid, NULL, 0);
    matrix2 = matrix1 + ROW1 * COL1;
    matrix3 = matrix2 + ROW2 * COL2;

    // initialize matrix1 with random values from 1 to 5
    for (int i = 0; i < ROW1 * COL1; i++) {
        matrix1[i] = rand() % 5 + 1;
    }

    // initialize matrix2 with random values from 1 to 4
    for (int i = 0; i < ROW2 * COL2; i++) {
        matrix2[i] = rand() % 4 + 1;
    }

    // calculate matrix multiplication
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            matrix3[i * COL2 + j] = 0;
            for (int k = 0; k < COL1; k++) {
                matrix3[i * COL2 + j] += matrix1[i * COL1 + k] * matrix2[k * COL2 + j];
            }
        }
    }

    // display result
    printf("Matrix 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            printf("%d ", matrix1[i * COL1 + j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("Matrix 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix2[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("Matrix 1 x Matrix 2:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\n");

    sleep(10);
    // detach and remove shared memory
    shmdt(matrix3);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
```

**Penjelasan:**

Digunakan konsep shared memory sebagai berikut.
```c
    int shmid;
    key_t key = 1234;
    int *matrix1, *matrix2, *matrix3;

    // allocate shared memory for matrices
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segments to matrices
    matrix1 = shmat(shmid, NULL, 0);
    matrix2 = matrix1 + ROW1 * COL1;
    matrix3 = matrix2 + ROW2 * COL2;

    sleep(10);

    // detach and remove shared memory
    shmdt(matrix3);
    shmctl(shmid, IPC_RMID, NULL);
```
- key 1234 adalah key yang akan digunakan untuk sharing memory
- id akan disimpan ke variabel integer menggunakan fungsi `shmget()` dengan memanggil `shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);`
- `int *matrix1` dan `*matrix2`digunakan untuk menyimpan matriks 1 dan 2 sedangkan `*matrix3` digunakan untuk menyimpan matriks hasil perkalian
- `sleep(10)` digunakan untuk menunggu program lain dijalankan (id shared memory sedang aktif)
- setelah shared memory selesai, variabel `int *matrix3` akan didestroy dan id akan dihapus

Sedangkan untuk pembuatan matriks dan perkaliannya seperti berikut.
```c
    srand(time(NULL));

// initialize matrix1 with random values from 1 to 5
    for (int i = 0; i < ROW1 * COL1; i++) {
        matrix1[i] = rand() % 5 + 1;
    }

    // initialize matrix2 with random values from 1 to 4
    for (int i = 0; i < ROW2 * COL2; i++) {
        matrix2[i] = rand() % 4 + 1;
    }

    // calculate matrix multiplication
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            matrix3[i * COL2 + j] = 0;
            for (int k = 0; k < COL1; k++) {
                matrix3[i * COL2 + j] += matrix1[i * COL1 + k] * matrix2[k * COL2 + j];
            }
        }
    }

    // display result
    printf("Matrix 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            printf("%d ", matrix1[i * COL1 + j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("Matrix 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix2[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("Matrix 1 x Matrix 2:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\n");
```
- `srand(time(NULL));` digunakan agar setiap run program, angkanya selalu dirandom ulang
- `rand()` untuk melakukan operasi random
#### Soal B
Buatlah program C kedua dengan nama `cinta.c`. Program ini akan mengambil variabel hasil perkalian matriks dari program `kalian.c` (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
**(Catatan: wajib menerapkan konsep shared memory)**

#### Soal C
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
**(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)**

**Penyelesaian:** 
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define ROW1 4
#define COL2 5

int kali(int num, int res[], int ressz){
    int carry = 0;
    for(int i=0; i<ressz; i++) {    	
        int produk = (res[i]*num) + carry;
        res[i] = produk%10;            
        carry = produk/10;     
    }
    while(carry) {                      
    	res[ressz] = carry%10;
    	carry/=10;
	ressz++;
    }
    return ressz;
}

void *fakt(void *args){
    int res[100];           
    int n;
    
    n = *((int *)args);
    res[0] = 1;
    int ressz = 1;
    for(int num = 2; num <= n; num++){
        ressz = kali(num, res, ressz);
    }
    for(int i = ressz - 1; i >= 0; i--){
        printf("%d", res[i]);
    }
    return NULL;
}


int main() {
    // stamp the start time to count execution time
    clock_t mulai, selesai;
    double waktu;
    mulai = clock();

    int shmid;
    key_t key = 1234;
    int *matrix3;

    // get shared memory segment for matrix3
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segment to matrix3
    matrix3 = shmat(shmid, NULL, 0);

    // display result
    printf("Matrix 1 x Matrix 2:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j + 18]);
        }
        printf("\n");
    }
    printf("\n");

    // calculate factorial and print the matrix
    printf("Matrix 1 x Matrix 2 (dengan faktorial):\n");
    pthread_t tid[ROW1][COL2];
    for(int i=0; i<ROW1; i++){
        for(int j=0; j<COL2; j++){
            pthread_create(&tid[i][j], NULL, fakt, &matrix3[i * COL2 + j + 18]);
            pthread_join(tid[i][j], NULL);
            printf(" ");
        }
        printf("\n");
    }
    printf("\n");

    // detach shared memory
    shmdt(matrix3);
    
    // stamp the end time and count the execution time
    selesai = clock();
    waktu = ((double) (selesai - mulai)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);

    return 0;
}

```
**Penjelasan:**

Digunakan shared memory untuk mengambil hasil perkalian matriks dari `kalian.c`.
```c
    int shmid;
    key_t key = 1234;
    int *matrix3;

    // get shared memory segment for matrix3
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segment to matrix3
    matrix3 = shmat(shmid, NULL, 0);

    // display result
    printf("Matrix 1 x Matrix 2:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j + 18]);
        }
        printf("\n");
    }
    printf("\n");

    // detach shared memory
    shmdt(matrix3);
```
- key yang dipakai harus sama seperti sebelumnya yakni 1234, lalu dibaca dan dimasukkan ke id shared memory program cinta ini melalui fungsi `shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);`
- lalu dimasukkan kembali ke variabel array dua dimensi di program ini, yakni variabel `int *matrix3;`
- ditampilkan lagi hasil kalinya untuk mengecek apakah hasil sudah cocok/sama dengan hasil dari `kalian.c`

Kemudian untuk perhitungan faktorial disini digunakan bantuan konsep thread dan multithreading untuk memanggil fungsi penghitung faktorial berikut.
```c
int kali(int num, int res[], int ressz){
    int carry = 0;
    for(int i=0; i<ressz; i++) {    	
        int produk = (res[i]*num) + carry;
        res[i] = produk%10;            
        carry = produk/10;     
    }
    while(carry) {                      
    	res[ressz] = carry%10;
    	carry/=10;
	ressz++;
    }
    return ressz;
}

void *fakt(void *args){
    int res[100];           
    int n;
    
    n = *((int *)args);
    res[0] = 1;
    int ressz = 1;
    for(int num = 2; num <= n; num++){
        ressz = kali(num, res, ressz);
    }
    for(int i = ressz - 1; i >= 0; i--){
        printf("%d", res[i]);
    }
    return NULL;
}
```
- faktorial ini dilakukan dengan cara disimpan ke array untuk setiap angkanya karena **worstcase** yang dihitung bisa mencapai **40!**
- untuk cara detail nya bisa dilihat melalui referensi berikut: https://www.geeksforgeeks.org/factorial-large-number/
- fungsi `fakt` adalah fungsi `void *` yang bertujuan untuk menyesuaikan penggunaan multithreading, sehingga argumennya juga harus berupa `void *` (perlu ditypecasting untuk dipakai)
- pada fungsi `fakt` akan dilakukan perhitungan faktorial dengan bantuan fungsi kali untuk perkaliannya

Fungsi tersebut dipanggil dengan cara sebagai berikut.
```c
    // calculate factorial and print the matrix
        printf("Matrix 1 x Matrix 2 (dengan faktorial):\n");
        pthread_t tid[ROW1][COL2];
        for(int i=0; i<ROW1; i++){
            for(int j=0; j<COL2; j++){
                pthread_create(&tid[i][j], NULL, fakt, &matrix3[i * COL2 + j + 18]);
                pthread_join(tid[i][j], NULL);
                printf(" ");
            }
            printf("\n");
        }
        printf("\n");
```
- `pthread_t tid[ROW1][COL2];` adalah id dari setiap thread yang akan digunakan, pada cara ini `tid` disimpan dalam array dua dimensi untuk memudahkan pengaksesan
- untuk setiap elemen matriks dilakukan faktorial dengan `tid` nya masing-masing melalui fungsi `pthread_create(&tid[i][j], NULL, fakt, &matrix3[i * COL2 + j + 18]);`
- lalu dilakukan join tiap thread agar hasil faktorial ditampilkan secara urut melalui fungsi `pthread_join(tid[i][j], NULL);`

Terakhir diperlukan algoritma untuk menghitung execution time dengan tujuan menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program multithread dengan yang tidak sebagai berikut.
```c
    // stamp the start time to count execution time
    clock_t mulai, selesai;
    double waktu;
    mulai = clock();
    .
    .
    // program content
    .
    .
    // stamp the end time and count the execution time
    selesai = clock();
    waktu = ((double) (selesai - mulai)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);
```
- `double waktu` digunakan untuk menyetor interval timestamp antara `clock_t mulai` dan `clock_t selesai`
#### Soal D
Buatlah program C ketiga dengan nama `sisop.c`. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.


**Penyelesaian:** 
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ROW1 4
#define COL2 5

int factorial(int n) {
    int j = 2; 
    int a[1000] = {0}; 
    a[0] = 1;  
    int len = 1; 
    int c = 0, num = 0;  
    while(j <= n)  
    {  
        c = 0;  
        num = 0;  
        while(c < len)  
        {  
            a[c] = a[c] * j;  
            a[c] = a[c] + num;  
            num = a[c] / 10;  
            a[c] = a[c] % 10;  
            c++;  
        }  
        while(num != 0)  
        {  
            a[len] = num % 10;  
            num = num / 10;  
            len++;  
        }  
        j++;  
    }  
    len--;  

    while(len >= 0)  
    {  
        printf("%d", a[len]);  
        len--;  
    }
    printf(" ");
}

int main() {
    // stamp the start time to count execution time
    clock_t mulai, selesai;
    double waktu;
    mulai = clock();

    int shmid;
    key_t key = 1234;
    int *matrix3;

    // get shared memory segment for matrix3
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segment to matrix3
    matrix3 = shmat(shmid, NULL, 0);

    // display result without factorial
    printf("Matrix 1 x Matrix 2 (tanpa faktorial):\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j + 18]);
        }
        printf("\n");
    }
    printf("\n");

    // calculate factorial and print the matrix
    printf("Matrix 1 x Matrix 2 (dengan faktorial):\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            factorial(matrix3[i * COL2 + j + 18]);
        }
        printf("\n");
    }

    // detach shared memory
    shmdt(matrix3);
    
    // stamp the end time and count execution time
    selesai = clock();
    waktu = ((double) (selesai - mulai)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);

    return 0;
}
```
**Penjelasan:**
- Program ini tujuannya sama persis dengan cinta.c namun tanpa bantuan thread dan multithreading


**Kesulitan Nomor 2 :**
- Awalnya program `cinta.c` tidak mengeluarkan hasil yang benar. Perlu diketahui bahwa program dengan konsep shared memory harus dijalankan secara bersamaan. Sehingga perlu ditambahkan `sleep(10)` sebagai jeda waktu agar kita sempat untuk menjalankan program kedua sebelum program pertama selesai dieksekusi.

| <p align="center"> kalian.c </p> | <p align="center"> cinta.c & sisop.c </p> |
| -------------------------------------------- | -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/output3.jpg" width = "400"/> | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/output4.jpg" width = "400"/> |

## Nomor 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

#### Soal A
Bantulah Elshe untuk membuat sistem *stream (receiver)* **stream.c** dengan *user (multiple sender dengan identifier)* **user.c** menggunakan **message queue (wajib)**. Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan **semua aktivitas sesuai perintah akan dikerjakan oleh sistem.**

**Penyelesaian:**

**stream.c**
```c
include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <cjson/cJSON.h>
#include <sys/msg.h>
#include <errno.h>
#include <sys/sem.h>

#define MAX_MSG_SIZE 100
#define MAX_USERS 2

//  gcc -o stream stream.c -lcjson

struct msgbuf
{
  long mtype;
  char mtext[MAX_MSG_SIZE];
};

int main(){

	// Create message queue and semaphore
	key_t stream_key = ftok("stream.c", 'M');
	int stream_queue = msgget(stream_key, IPC_CREAT | 0666);
	if (stream_queue == -1){
		perror("msgget failed");
	    exit(1);
	}

	key_t sem_key = ftok("user.c", 'S');
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}

  // Initialize semaphore to 2 (maximum number of users)
  	union semun{
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	} arg;
  
  	arg.val = MAX_USERS;
	  
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl failed");
   		exit(1);
	}
	
	while (1){
		
	    struct msgbuf msg;
	    int len = msgrcv(stream_queue, &msg, MAX_MSG_SIZE, 0, 0);
	    if (len == -1){
	    	perror("msgrcv failed");
	    	exit(1);
	    }
	
	    // Wait for semaphore
	    struct sembuf sb = {0, -1, 0};
	    if (semop(sem_id, &sb, 1) == -1)
	    {
			perror("semop failed");
			exit(1);
	    }
	    
	    // Parse user command
	    char *command = msg.mtext;
	    char *param = strchr(command, ' ');
	    if (param){
	    	*param = '\0';
	    	param++;
	    	while (isspace(*param)){
        		param++;
      		}
    	}
    }
}
```

**user.c**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/sem.h>

#define MAX_MSG_SIZE 100

//gcc -o user user.c -lpthread

struct msgbuf {
    long mtype;
    char mtext[MAX_MSG_SIZE];
};

int main() {
	
	// Create message queue and semaphore
	key_t stream_key = ftok("stream.c", 'M');
	if(stream_key == -1){
		perror("ftok failed");
	    exit(1);	
	}
	
	int stream_queue = msgget(stream_key, IPC_CREAT | 0666);
	if (stream_queue == -1){
		perror("msgget failed");
	    exit(1);
	}
	
	key_t sem_key = ftok("user.c", 'S');
	if(sem_key == -1){
		perror("ftok failed");
	    exit(1);	
	}
	
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}
	
	union semun {
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	};
	

	union semun arg;
	struct sembuf sb = {0, -1, 0}; // Wait operation
	
	arg.val = 2;
	
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl");
    	exit(1);
	}

	// Send messages to stream.c
  	char message[MAX_MSG_SIZE];
  	
  	pid_t p = getpid();
	printf("User %d connected to stream.\n", p);
}

```

**Penjelasan:**

1. Menginisialisasi variabel msgid untuk mendapatkan message id dari pihak stream dan user menggunakan msgget.
2. Mengirimkan command menggunakan msgsnd pada file user.c dan diterima file stream.c dengan menggunakan msgrcv.
NOTE: Penerapan message queue ini dimodifikasi dari hasil generate code di ChatGPT.

#### Soal B
User pertama kali akan mengirimkan perintah **DECRYPT** kemudian sistem *stream* akan melakukan *decrypt/decode/*konversi pada file **song-playlist.json** (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-*output*-kannya menjadi **playlist.txt** diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program **stream.c tanpa menggunakan koneksi socket** sehingga struktur direktorinya adalah sebagai berikut:
```
└── soal3
    ├── playlist.txt
    ├── song-playlist.json
    ├── stream.c
    └── user.c
```

**Penyelesaian:**

**stream.c**
```c
//DECRYPT	
		if (!strcmp(command, "DECRYPT")){
			FILE *fp = fopen("song-playlist.json", "r");
			if (fp == NULL){
        		printf("Error opening file: %s\n", "song-playlist.json");
        		return 1;
      		}
      		
      		fseek(fp, 0, SEEK_END);
		    long file_size = ftell(fp);
		    fseek(fp, 0, SEEK_SET);
		    
      		char *json_str = malloc(file_size + 1);
		    if (json_str == NULL){
		        printf("Error allocating memory\n");
		        return 1;
		    }
	      	fread(json_str, 1, file_size, fp);
	      	fclose(fp);
	      	json_str[file_size] = '\0';

	      	cJSON *root = cJSON_Parse(json_str);
	      	if (root == NULL){
	        	printf("Error parsing JSON: %s\n", cJSON_GetErrorPtr());
	        	free(json_str);
	        	return 1;
	      	}

      		FILE *playlist = fopen("playlist.txt", "w");
      		if (playlist == NULL){
		        printf("Error opening playlist file for writing\n");
		        cJSON_Delete(root);
		        free(json_str);
		        return 1;
      		}

		    cJSON *item = NULL;
		    cJSON_ArrayForEach(item, root){
		        cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
		        cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");
				
        		if (strcmp(method->valuestring, "base64") == 0){
			        char *Dcd = base64(song->valuestring);
			        if (Dcd == NULL){
			        	printf("Error decoding base64: %s\n", song->valuestring);
			            continue;
			        }
			        printf("Base64: %s\n", Dcd);
			        fprintf(playlist, "%s\n", Dcd);
			        free(Dcd);
		        }
		        
		        else if (strcmp(method->valuestring, "hex") == 0){
		        	char *Dcd = hex(song->valuestring);
		        	if (Dcd == NULL){
		            	printf("Error decoding hex: %s\n", song->valuestring);
		            	continue;
		          	}
		        	printf("Hex: %s\n", Dcd);
		        	fprintf(playlist, "%s\n", Dcd);
		        	free(Dcd);
		        }
		        
        		else if (strcmp(method->valuestring, "rot13") == 0){
			        char *Dcd = rot13(song->valuestring);
			        if (Dcd == NULL){
			            printf("Error decoding rot13: %s\n", song->valuestring);
			            continue;
			        }
			        printf("Rot13: %s\n", Dcd);
			        fprintf(playlist, "%s\n", Dcd);
			        free(Dcd);
		        }
		        
        		else{
		        	printf("Unknown method: %s\n", method->valuestring);
		        	continue;
		        }
      		}

	    	fclose(playlist);
	    	cJSON_Delete(root);
	    	free(json_str);
    	}
```

**user.c**
```c
        if(!strcmp(message, "DECRYPT")){
	    	strncpy(msg.mtext, message, MAX_MSG_SIZE);
		}
```

**Penjelasan:**
1. User akan mengirimkan perintah **"DECRYPT"** kepada file stream.c yang kemudian file **song-playlist.json** akan dilakukan dekripsi berdasarkan metode yang diberikan oleh file tersebut.
2. Untuk saling mengirim perintah satu sama lain, digunakan konsep _message queue_ seperti yang dijelaskan pada poin A.
3. Potongan kode diatas yang diberikan oleh file stream.c akan melakukan dekripsi kepada file **song-playlist.json**, langkah pertamanya adalah dengan membuka file **song-playlist.json**
```c
FILE *fp = fopen("song-playlist.json", "r");
if (fp == NULL){
    printf("Error opening file: %s\n", "song-playlist.json");
    return 1;
}
```
Potongan kode ini membuka file **"song-playlist.json"** dalam mode baca ("r") dan melakukan pengecekan apakah file berhasil dibuka. Jika gagal, akan ditampilkan pesan error dan program berhenti.
4. Menghitung ukuran file dan mengalokasikan memori untuk menyimpan isi file:
```c
fseek(fp, 0, SEEK_END);
long file_size = ftell(fp);
fseek(fp, 0, SEEK_SET);

char *json_str = malloc(file_size + 1);
if (json_str == NULL){
    printf("Error allocating memory\n");
    return 1;
}
```
Potongan kode ini menggunakan fungsi `fseek` untuk memindahkan posisi penunjuk file ke akhir file dan menghitung ukuran file menggunakan fungsi `ftell`. Kemudian, memori dialokasikan untuk menyimpan isi file dengan ukuran yang tepat.
5. Membaca isi file ke dalam string `json_str` dan menutup file:
```c
fread(json_str, 1, file_size, fp);
fclose(fp);
json_str[file_size] = '\0';
```
Potongan kode ini menggunakan fungsi `fread` untuk membaca isi file ke dalam string `json_str`. Setelah itu, file ditutup dan karakter null ('\0') ditambahkan pada akhir string untuk menandakan akhir dari string.
6. Membuat objek cJSON dari string JSON yang telah dibaca:
```c
cJSON *root = cJSON_Parse(json_str);
if (root == NULL){
    printf("Error parsing JSON: %s\n", cJSON_GetErrorPtr());
    free(json_str);
    return 1;
}
```
Potongan kode ini menggunakan fungsi `cJSON_Parse` dari library cJSON untuk memparsing string JSON menjadi objek cJSON. Jika parsing gagal, pesan error akan ditampilkan dan memori yang dialokasikan untuk `json_str` akan dibebaskan.
7. Membuka file "playlist.txt" untuk ditulis:
```c
FILE *playlist = fopen("playlist.txt", "w");
if (playlist == NULL){
    printf("Error opening playlist file for writing\n");
    cJSON_Delete(root);
    free(json_str);
    return 1;
}
```
Potongan kode ini membuka file "playlist.txt" dalam mode tulis ("w") dan melakukan pengecekan apakah file berhasil dibuka. Jika gagal, akan ditampilkan pesan error, objek cJSON akan dihapus, dan memori `json_str` akan dibebaskan.
8. Melakukan dekripsi dan menulis hasil dekripsi ke dalam file "playlist.txt":
```c
cJSON *item = NULL;
cJSON_ArrayForEach(item, root){
    cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
    cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");

    // Mendekripsi berdasarkan metode yang ditentukan
    if (strcmp(method->valuestring, "base64") == 0){
       
        char *Dcd = base64(song->valuestring);
        if (Dcd == NULL){
            printf("Error decoding base64: %s\n", song->valuestring);
            continue;
        }
        printf("Base64: %s\n", Dcd);
        fprintf(playlist, "%s\n", Dcd);
        free(Dcd);
    }
    else if (strcmp(method->valuestring, "hex") == 0){
        char *Dcd = hex(song->valuestring);
        if (Dcd == NULL){
            printf("Error decoding hex: %s\n", song->valuestring);
            continue;
        }
        printf("Hex: %s\n", Dcd);
        fprintf(playlist, "%s\n", Dcd);
        free(Dcd);
    }
    else if (strcmp(method->valuestring, "rot13") == 0){
        char *Dcd = rot13(song->valuestring);
        if (Dcd == NULL){
            printf("Error decoding rot13: %s\n", song->valuestring);
            continue;
        }
        printf("Rot13: %s\n", Dcd);
        fprintf(playlist, "%s\n", Dcd);
        free(Dcd);
    }
    else{
        printf("Unknown method: %s\n", method->valuestring);
        continue;
    }
}
```
Potongan kode ini merupakan bagian dari loop yang mengiterasi setiap objek dalam array JSON. Pada setiap iterasi, potongan kode ini melakukan dekripsi berdasarkan metode yang ditentukan dalam JSON, seperti base64, hex, atau rot13. Hasil dekripsi kemudian ditulis ke dalam file "playlist.txt" menggunakan `fprintf`. Di akhir setiap iterasi, memori yang dialokasikan untuk hasil dekripsi (`Dcd`) akan dibebaskan menggunakan `free`.
9. Menutup file "playlist.txt" dan membersihkan memori:
```c
fclose(playlist);
cJSON_Delete(root);
free(json_str);
```
Potongan kode ini menutup file "playlist.txt" setelah selesai menulis. Selanjutnya, objek cJSON dan memori yang dialokasikan untuk json_str juga dibersihkan dengan menggunakan cJSON_Delete dan free.

Potongan kode ini secara keseluruhan bertujuan untuk membaca file JSON, melakukan dekripsi pada setiap entitas dalam file JSON, dan menyimpan hasil dekripsi ke dalam file "playlist.txt". Metode dekripsi yang digunakan seperti base64, hex, dan rot13 dapat disesuaikan dengan nilai dari objek JSON.

Untuk implementasi fungsi dekripsi ROT13, Base64, dan Hex adalah sebagai berikut:
```c
static char* rot13(const char* input)
{
    const int len = strlen(input);
    char* output = (char*)malloc(len + 1);
    if (output == NULL) {
        return NULL;
    }

    for (int i = 0; i < len; i++) {
        char c = input[i];
        if (isalpha(c)) {
            char base = islower(c) ? 'a' : 'A';
            c = base + (c - base + 13) % 26;
        }
        output[i] = c;
    }
    output[len] = '\0';
    return output;
}

//static const unsigned char base64_table[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/'};
static const unsigned char base64_table[256] = {['A'] = 0, ['B'] = 1, ['C'] = 2, ['D'] = 3, ['E'] = 4, ['F'] = 5, ['G'] = 6, ['H'] = 7, ['I'] = 8, ['J'] = 9, ['K'] = 10, ['L'] = 11, ['M'] = 12, ['N'] = 13, ['O'] = 14, ['P'] = 15, ['Q'] = 16, ['R'] = 17, ['S'] = 18, ['T'] = 19, ['U'] = 20, ['V'] = 21, ['W'] = 22, ['X'] = 23, ['Y'] = 24, ['Z'] = 25, ['a'] = 26, ['b'] = 27, ['c'] = 28, ['d'] = 29, ['e'] = 30, ['f'] = 31, ['g'] = 32, ['h'] = 33, ['i'] = 34, ['j'] = 35, ['k'] = 36, ['l'] = 37, ['m'] = 38, ['n'] = 39, ['o'] = 40, ['p'] = 41, ['q'] = 42, ['r'] = 43, ['s'] = 44, ['t'] = 45, ['u'] = 46, ['v'] = 47, ['w'] = 48, ['x'] = 49, ['y'] = 50, ['z'] = 51, ['0'] = 52, ['1'] = 53, ['2'] = 54, ['3'] = 55, ['4'] = 56, ['5'] = 57, ['6'] = 58, ['7'] = 59, ['8'] = 60, ['9'] = 61, ['+'] = 62, ['/'] = 63};

// Base64 decoding function
static char *base64(const char *input)
{
  int len = strlen(input);
  if (len % 4 != 0)
  {
    printf("Input length is not a multiple of 4\n");
    return NULL;
  }

  int output_len = 3 * (len / 4);
  if (input[len - 1] == '=')
    output_len--;
  if (input[len - 2] == '=')
    output_len--;

  char *output = malloc(output_len + 1);
  if (output == NULL)
  {
    printf("Failed to allocate memory for output\n");
    return NULL;
  }

  char *output_ptr = output;
  for (int i = 0; i < len; i += 4)
  {
    unsigned char values[4] = {
        base64_table[(unsigned char)input[i]],
        base64_table[(unsigned char)input[i + 1]],
        base64_table[(unsigned char)input[i + 2]],
        base64_table[(unsigned char)input[i + 3]]};

    *output_ptr++ = (values[0] << 2) | (values[1] >> 4);
    if (input[i + 2] != '=')
      *output_ptr++ = (values[1] << 4) | (values[2] >> 2);
    if (input[i + 3] != '=')
      *output_ptr++ = (values[2] << 6) | values[3];
  }

  *output_ptr = '\0';
  return output;
}

static char *hex(const char *input)
{
  int len = strlen(input);
  if (len % 2 != 0)
  {
    return NULL;
  }

  int output_len = len / 2;
  char *output = malloc(output_len + 1);
  if (output == NULL)
  {
    return NULL;
  }

  char *output_ptr = output;
  for (int i = 0; i < len; i += 2)
  {
    char hex[3] = {input[i], input[i + 1], '\0'};
    int value = strtol(hex, NULL, 16);
    *output_ptr++ = (char)value;
  }

  *output_ptr = '\0';
  return output;
}
```
- **Fungsi rot13**:
Fungsi ini mengimplementasikan algoritma ROT13 (rotate by 13 places). Fungsi menerima input dalam bentuk string (const char* input) dan mengembalikan hasil dekripsi dalam bentuk string (char*). Algoritma ROT13 menggeser setiap huruf sejauh 13 posisi dalam alfabet. Fungsi ini melakukan iterasi pada setiap karakter dalam input dan memeriksa apakah karakter tersebut merupakan huruf. Jika ya, karakter tersebut digeser sejauh 13 posisi dalam alfabet (dalam huruf kecil atau huruf besar). Karakter hasil dekripsi kemudian disimpan dalam array output. Fungsi ini mengembalikan hasil dekripsi yang telah disimpan dalam output.

- **Fungsi base64:**
Fungsi ini digunakan untuk melakukan dekripsi dari format base64. Fungsi menerima input dalam bentuk string (const char* input) dan mengembalikan hasil dekripsi dalam bentuk string (char*). Fungsi ini melakukan dekripsi base64 dengan mengonversi setiap blok 4 karakter input menjadi blok 3 karakter output. Proses dekripsi dilakukan dengan menggunakan tabel base64_table yang mengandung mapping karakter base64 ke nilai numerik. Fungsi ini melakukan iterasi pada setiap blok 4 karakter dalam input, mengkonversi karakter-karakter tersebut ke nilai numerik dengan menggunakan tabel base64_table, dan menggabungkan nilai-nilai numerik tersebut untuk membentuk karakter hasil dekripsi. Karakter hasil dekripsi kemudian disimpan dalam array output. Fungsi ini mengembalikan hasil dekripsi yang telah disimpan dalam output.

- **Fungsi hex:**
Fungsi ini digunakan untuk melakukan dekripsi dari format heksadesimal. Fungsi menerima input dalam bentuk string (const char* input) dan mengembalikan hasil dekripsi dalam bentuk string (char*). Fungsi ini melakukan iterasi pada setiap blok 2 karakter dalam input, mengonversi karakter-karakter tersebut menjadi nilai numerik dengan menggunakan fungsi strtol, dan menggabungkan nilai-nilai numerik tersebut untuk membentuk karakter hasil dekripsi. Karakter hasil dekripsi kemudian disimpan dalam array output. Fungsi ini mengembalikan hasil dekripsi yang telah disimpan dalam output.

#### Soal C
Selain itu, *user* dapat mengirimkan perintah **LIST**, kemudian sistem *stream* akan menampilkan daftar lagu yang telah di-*decrypt*
**Sample Output:**
```
17 - MK
1-800-273-8255 - Logic
1950 - King Princess
…
Your Love Is My Drug - Kesha
YOUTH - Troye Sivan
ZEZE (feat. Travis Scott & Offset) - Kodak Black

```
**Penyelesaian:**

**stream.c**
```c
//LIST
		else if (!strcmp(command, "LIST")){
			FILE *pl = fopen("playlist.txt", "r");
			if (pl == NULL){
				printf("Error opening playlist file for reading\n");
				exit(1);
			}
			 fclose(pl);
			 system("sort playlist.txt");
  		}
```

**user.c**
```c
        else if(!strcmp(message, "LIST")){
	    	strncpy(msg.mtext, message, MAX_MSG_SIZE);
		}
```

**Penjelasan:**
1. membuka file "playlist.txt" dalam mode baca ("r") dan menyimpan file tersebut dalam variabel `pl` yang merupakan pointer ke tipe `FILE`. Jika file berhasil dibuka, maka pointer `pl` akan menunjuk ke file tersebut. Jika terjadi kesalahan saat membuka file, misalnya file tidak ditemukan, maka akan dicetak pesan kesalahan **"Error opening playlist file for reading"** dan program akan keluar `(exit(1))`.
2. `system("sort playlist.txt");` akan menjalankan perintah shell "sort" pada file "playlist.txt". Perintah "sort" digunakan untuk mengurutkan isi file berdasarkan urutan leksikografis. Dengan menjalankan perintah ini, konten dari file "playlist.txt" akan diurutkan dan dicetak ke output.

#### Soal D
_User_ juga dapat mengirimkan perintah **PLAY <SONG>** dengan ketentuan sebagai berikut
```
PLAY "Stereo Heart"
    sistem akan menampilkan: 
    USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
PLAY "BREAK"
    sistem akan menampilkan:
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":
PLAY "UVUWEVWEVWVE"
    THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"

```
**Untuk mempermudah dan memperpendek kodingan, query bersifat tidak case sensitive 😀**

**Penyelesaian:**

**stream.c**
```c
//PLAY
		else if (!strcmp(command, "PLAY")){
			
			char *query = param;
		    int num_matches = 0;
		    char matches[1000][100];
			char songnm[100];
			
		    FILE *pl = fopen("playlist.txt", "r");
		    
		    if (pl == NULL){
		        printf("Error opening playlist file for reading\n");
		        exit(1);
		    }

		    while (fgets(songnm, 100, pl) != NULL)
		    {
		        // Remove trailing newline if present
		        songnm[strcspn(songnm, "\n")] = '\0';
		
		        // Check if song name contains the query
		        char *found = strcasestr(songnm, query);
		        if (found != NULL)
		        {
		            strcpy(matches[num_matches], songnm);
		            num_matches+=1;
		        }
		        
		    }
		
		    fclose(pl);
		
		    if (num_matches == 0){
		        printf("THERE IS NO SONG CONTAINING \"%s\"\n", query);
		    }
		    
		    else if (num_matches == 1){
		         printf("USER <%d> PLAYING \"%s\"\n", getpid(), matches[0]);
		    }
		    
		    else{
		        printf("THERE ARE \"%d\" SONG(S) CONTAINING \"%s\":\n", num_matches, query);
		        for (int i = 0; i < num_matches; i++){
		            printf("%d. %s\n", i + 1, matches[i]);
		        }
		    }
  		}
```

**user.c**
```c
        else if(!strncmp(message, "PLAY", 4)){
			
			char *as = strtok(message + 5, "\"");
		    if (as == NULL){
		        printf("Invalid Parameter: %s\n", message);
		        continue;
		    }
		    // Convert song to uppercase for case-insensitive comparison
		    for (int i = 0; i < strlen(as); i++){
		        as[i] = toupper(as[i]);
		    }

	    	sprintf(msg.mtext, "PLAY %s", as);
        }
```

**Penjelasan:**
1. `char *query = param;`
   Baris ini mendeklarasikan variabel `query` sebagai pointer ke string yang memiliki nilai yang sama dengan `param`. Variabel `param` adalah argumen perintah yang digunakan untuk mencari lagu dalam daftar playlist.
2. `int num_matches = 0;`
   Baris ini mendeklarasikan variabel `num_matches` sebagai integer yang akan digunakan untuk menghitung jumlah lagu yang cocok dengan kueri yang diberikan.
3. `char matches[1000][100];`
   Baris ini mendeklarasikan array dua dimensi `matches` dengan ukuran 1000x100. Array ini digunakan untuk menyimpan daftar lagu yang cocok dengan kueri.
4. `char songnm[100];`
   Baris ini mendeklarasikan array `songnm` dengan ukuran 100. Array ini digunakan untuk menyimpan nama lagu yang dibaca dari file playlist.
5. `FILE *pl = fopen("playlist.txt", "r");`
   Baris ini membuka file "playlist.txt" dalam mode baca ("r") dan menyimpan file tersebut dalam variabel `pl` yang merupakan pointer ke tipe `FILE`. Jika file berhasil dibuka, maka pointer `pl` akan menunjuk ke file tersebut. Jika terjadi kesalahan saat membuka file, misalnya file tidak ditemukan, maka akan dicetak pesan kesalahan "Error opening playlist file for reading" dan program akan keluar (`exit(1)`).
6. `while (fgets(songnm, 100, pl) != NULL) { ... }`
   Bagian ini melakukan loop untuk membaca setiap baris dari file "playlist.txt" menggunakan fungsi `fgets`. Setiap baris yang dibaca disimpan dalam array `songnm`.
7. `songnm[strcspn(songnm, "\n")] = '\0';`
   Baris ini menghapus karakter newline (`'\n'`) dari string `songnm` jika ada. Hal ini dilakukan dengan menggunakan fungsi `strcspn` yang mencari posisi karakter newline dan kemudian menggantinya dengan karakter null (`'\0'`).
8. `char *found = strcasestr(songnm, query);`
   Baris ini mencari kueri yang diberikan dalam string `songnm` menggunakan fungsi `strcasestr`. Fungsi ini mencari kecocokan string secara case-insensitive, artinya tidak memperhatikan huruf besar atau kecil.
9. `if (found != NULL) { ... }`
   Bagian ini memeriksa apakah kueri yang diberikan ditemukan dalam string `songnm`. Jika ditemukan, nama lagu akan disalin ke dalam array `matches`, dan `num_matches` akan diincrement.
10. `fclose(pl);`
    Baris ini menutup file "playlist.txt" setelah selesai digunakan. Hal ini penting untuk memastikan bahwa sumber daya file telah dibebaskan.
11. `if (num_matches == 0) { ... }`
    Bagian ini memeriksa apakah tidak ada lagu yang cocok dengan kueri yang diberikan. Jika `num_matches` sama dengan 0, artinya tidak ada lagu yang cocok, maka akan dicetak "THERE IS NO SONG CONTAINING <NAMA LAGU YANG DIINPUTKAN>".
12. `else if (num_matches == 1) { ... }`
    Bagian ini memeriksa apakah hanya ada satu lagu yang cocok dengan kueri yang diberikan. Jika `num_matches` sama dengan 1, artinya hanya ada satu lagu yang cocok, maka akan dicetak pesan "USER \<pid> PLAYING "\<nama lagu\>"". \<pid\> akan diganti dengan nomor proses yang sedang berjalan, dan \<nama lagu\> akan diganti dengan nama lagu yang cocok.
13. `else { ... }`
    Bagian ini akan dieksekusi jika ada lebih dari satu lagu yang cocok dengan kueri yang diberikan. Pada bagian ini, akan dicetak pesan "THERE ARE "\<jumlah lagu\>" SONG(S) CONTAINING "\<kueri\>":" dan dilanjutkan dengan mencetak daftar lagu yang cocok dengan nomor urut.


#### Soal E
_User_ juga dapat menambahkan lagu ke dalam _playlist_ dengan syarat sebagai berikut:

1. User mengirimkan perintah
```
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>
```
2. _User_ dapat mengedit _playlist_ secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-_output_-kan **“SONG ALREADY ON PLAYLIST”**

**Penyelesaian:**

**stream.c**
```c

        void add_song(char *song_name){
            // Open the playlist file for reading
            FILE *playlist = fopen("playlist.txt", "r");
            if (playlist == NULL)
            {
                printf("Error opening playlist file for reading\n");
                return;
            }

            // Check if song is already in the playlist
            char line[100];
            while (fgets(line, 100, playlist) != NULL)
            {
                line[strcspn(line, "\n")] = '\0'; // Remove newline character
                if (strcasecmp(line, song_name) == 0)
                {
                    printf("SONG ALREADY ON PLAYLIST\n");
                    fclose(playlist);
                    return;
                }
            }

            // Close the file
            fclose(playlist);

            // Open the playlist file for appending
            playlist = fopen("playlist.txt", "a");
            if (playlist == NULL)
            {
                printf("Error opening playlist file for appending\n");
                return;
            }

            // Write song to the playlist
            fprintf(playlist, "%s\n", song_name);
            printf("USER <%d> ADD %s\n", getpid(), song_name);

            // Close the file
            fclose(playlist);
        }

//ADD
		else if (!strcmp(command, "ADD")){
			add_song(param);
		}

```

**user.c**
```c
        else if(!strncmp(message, "ADD", 3)){
			
			char *as = strtok(message + 4, "\n");
		    if (as == NULL){
		        printf("Invalid Parameter: %s\n", message);
		        continue;
		    }
	    	sprintf(msg.mtext, "ADD %s", as);
		}

```

**Penjelasan:**
1. Membuka file playlist (`playlist.txt`) dalam mode baca (`"r"`):
   ```c
   FILE *playlist = fopen("playlist.txt", "r");
   ```
2. Memeriksa apakah file playlist berhasil dibuka. Jika tidak berhasil, maka akan dicetak pesan error dan fungsi akan mengembalikan kontrol:
   ```c
   if (playlist == NULL)
   {
       printf("Error opening playlist file for reading\n");
       return;
   }
   ```
3. Mengecek apakah lagu sudah ada dalam playlist. Ini dilakukan dengan membaca setiap baris dalam file playlist dan membandingkannya dengan lagu yang ingin ditambahkan:
   ```c
   while (fgets(line, 100, playlist) != NULL)
   {
       line[strcspn(line, "\n")] = '\0'; // Menghapus karakter newline
       if (strcasecmp(line, song_name) == 0)
       {
           printf("SONG ALREADY ON PLAYLIST\n");
           fclose(playlist);
           return;
       }
   }
   ```
4. Menutup file playlist yang dibuka dalam mode baca:
   ```c
   fclose(playlist);
   ```
5. Membuka kembali file playlist dalam mode append (`"a"`):
   ```c
   playlist = fopen("playlist.txt", "a");
   ```
6. Memeriksa apakah file playlist berhasil dibuka kembali. Jika tidak berhasil, maka akan dicetak pesan error dan fungsi akan mengembalikan kontrol:
   ```c
   if (playlist == NULL)
   {
       printf("Error opening playlist file for appending\n");
       return;
   }
   ```
7. Menulis lagu ke dalam playlist dengan menggunakan fungsi `fprintf`:
   ```c
   fprintf(playlist, "%s\n", song_name);
   ```
8. Mencetak pesan sukses yang berisi informasi pengguna yang menambahkan lagu:
   ```c
   printf("USER <%d> ADD %s\n", getpid(), song_name);
   ```
9. Menutup file playlist yang telah ditambahkan lagunya:
   ```c
   fclose(playlist);
   ```
Dengan demikian, fungsi `add_song` ini akan membuka file playlist, memeriksa apakah lagu sudah ada dalam playlist, menambahkan lagu jika belum ada, dan menutup file playlist setelah penambahan lagu selesai.


### Soal F
Karena Elshe hanya memiliki _resource_ yang kecil, untuk saat ini Elshe hanya dapat memiliki dua _user_. Gunakan **semaphore (wajib)** untuk membatasi user yang mengakses _playlist_.
_Output_-kan **"STREAM SYSTEM OVERLOAD"** pada sistem ketika _user_ ketiga mengirim perintah apapun.

**Penyelesaian:**

**stream.c**
```c
    key_t sem_key = ftok("user.c", 'S');
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}

  // Initialize semaphore to 2 (maximum number of users)
  	union semun{
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	} arg;
  
  	arg.val = MAX_USERS;
	  
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl failed");
   		exit(1);
	}
    while(1){
        // Wait for semaphore
	    struct sembuf sb = {0, -1, 0};
	    if (semop(sem_id, &sb, 1) == -1)
	    {
			perror("semop failed");
			exit(1);
	    }
    }
```

**user.c**
```c
    key_t sem_key = ftok("user.c", 'S');
	if(sem_key == -1){
		perror("ftok failed");
	    exit(1);	
	}
	
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}
	
	union semun {
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	};

	union semun arg;
	struct sembuf sb = {0, -1, 0}; // Wait operation
	
	arg.val = 2;
	
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl");
    	exit(1);
	}
    while (1) {
	    // Wait for semaphore to become available
	    struct sembuf sem_wait = {0, -1, 0};
	    semop(sem_id, &sem_wait, 1);
        ... // commands
        // Release semaphore
	    struct sembuf sem_signal = {0, 1, 0};
	    semop(sem_id, &sem_signal, 1);
    }

    // Remove semaphore
  
    semctl(sem_id, 0, IPC_RMID, arg);
```

### Soal G
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan **"UNKNOWN COMMAND".**

**Penyelesaian:**

**user.c**
```c
        else{
			printf("UNKNOWN COMMAND\n");
		    continue;
		}
```

**Penjelasan:**
1. Apabila perintah yang diinputkan bukan berupa DECRYPT, LIST, ADD, atau, PLAY maka program akan mengoutputkan **UNKNOWN COMMAND.**

**Catatan:**
- Untuk mengerjakan soal ini dapat menggunakan contoh implementasi _message queue_ pada modul.
- Perintah DECRYPT akan melakukan decrypt/decode/konversi dengan metode ROT13, Base64, dan Hex yang sudah dijelaskan pada soal.

#### Hasil Akhir Penyelesaian:

**stream.c**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <cjson/cJSON.h>
#include <sys/msg.h>
#include <errno.h>
#include <sys/sem.h>

#define MAX_MSG_SIZE 100
#define MAX_USERS 2

//  gcc -o stream stream.c -lcjson

struct msgbuf
{
  long mtype;
  char mtext[MAX_MSG_SIZE];
};

static char* rot13(const char* input)
{
    const int len = strlen(input);
    char* output = (char*)malloc(len + 1);
    if (output == NULL) {
        return NULL;
    }

    for (int i = 0; i < len; i++) {
        char c = input[i];
        if (isalpha(c)) {
            char base = islower(c) ? 'a' : 'A';
            c = base + (c - base + 13) % 26;
        }
        output[i] = c;
    }
    output[len] = '\0';
    return output;
}

//static const unsigned char base64_table[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/'};
static const unsigned char base64_table[256] = {['A'] = 0, ['B'] = 1, ['C'] = 2, ['D'] = 3, ['E'] = 4, ['F'] = 5, ['G'] = 6, ['H'] = 7, ['I'] = 8, ['J'] = 9, ['K'] = 10, ['L'] = 11, ['M'] = 12, ['N'] = 13, ['O'] = 14, ['P'] = 15, ['Q'] = 16, ['R'] = 17, ['S'] = 18, ['T'] = 19, ['U'] = 20, ['V'] = 21, ['W'] = 22, ['X'] = 23, ['Y'] = 24, ['Z'] = 25, ['a'] = 26, ['b'] = 27, ['c'] = 28, ['d'] = 29, ['e'] = 30, ['f'] = 31, ['g'] = 32, ['h'] = 33, ['i'] = 34, ['j'] = 35, ['k'] = 36, ['l'] = 37, ['m'] = 38, ['n'] = 39, ['o'] = 40, ['p'] = 41, ['q'] = 42, ['r'] = 43, ['s'] = 44, ['t'] = 45, ['u'] = 46, ['v'] = 47, ['w'] = 48, ['x'] = 49, ['y'] = 50, ['z'] = 51, ['0'] = 52, ['1'] = 53, ['2'] = 54, ['3'] = 55, ['4'] = 56, ['5'] = 57, ['6'] = 58, ['7'] = 59, ['8'] = 60, ['9'] = 61, ['+'] = 62, ['/'] = 63};

// Base64 decoding function
static char *base64(const char *input)
{
  int len = strlen(input);
  if (len % 4 != 0)
  {
    printf("Input length is not a multiple of 4\n");
    return NULL;
  }

  int output_len = 3 * (len / 4);
  if (input[len - 1] == '=')
    output_len--;
  if (input[len - 2] == '=')
    output_len--;

  char *output = malloc(output_len + 1);
  if (output == NULL)
  {
    printf("Failed to allocate memory for output\n");
    return NULL;
  }

  char *output_ptr = output;
  for (int i = 0; i < len; i += 4)
  {
    unsigned char values[4] = {
        base64_table[(unsigned char)input[i]],
        base64_table[(unsigned char)input[i + 1]],
        base64_table[(unsigned char)input[i + 2]],
        base64_table[(unsigned char)input[i + 3]]};

    *output_ptr++ = (values[0] << 2) | (values[1] >> 4);
    if (input[i + 2] != '=')
      *output_ptr++ = (values[1] << 4) | (values[2] >> 2);
    if (input[i + 3] != '=')
      *output_ptr++ = (values[2] << 6) | values[3];
  }

  *output_ptr = '\0';
  return output;
}

static char *hex(const char *input)
{
  int len = strlen(input);
  if (len % 2 != 0)
  {
    return NULL;
  }

  int output_len = len / 2;
  char *output = malloc(output_len + 1);
  if (output == NULL)
  {
    return NULL;
  }

  char *output_ptr = output;
  for (int i = 0; i < len; i += 2)
  {
    char hex[3] = {input[i], input[i + 1], '\0'};
    int value = strtol(hex, NULL, 16);
    *output_ptr++ = (char)value;
  }

  *output_ptr = '\0';
  return output;
}

void add_song(char *song_name){
    // Open the playlist file for reading
    FILE *playlist = fopen("playlist.txt", "r");
    if (playlist == NULL)
    {
        printf("Error opening playlist file for reading\n");
        return;
    }

    // Check if song is already in the playlist
    char line[100];
    while (fgets(line, 100, playlist) != NULL)
    {
        line[strcspn(line, "\n")] = '\0'; // Remove newline character
        if (strcasecmp(line, song_name) == 0)
        {
            printf("SONG ALREADY ON PLAYLIST\n");
            fclose(playlist);
            return;
        }
    }

    // Close the file
    fclose(playlist);

    // Open the playlist file for appending
    playlist = fopen("playlist.txt", "a");
    if (playlist == NULL)
    {
        printf("Error opening playlist file for appending\n");
        return;
    }

    // Write song to the playlist
    fprintf(playlist, "%s\n", song_name);
    printf("USER <%d> ADD %s\n", getpid(), song_name);

    // Close the file
    fclose(playlist);
}


int main(){

	// Create message queue and semaphore
	key_t stream_key = ftok("stream.c", 'M');
	int stream_queue = msgget(stream_key, IPC_CREAT | 0666);
	if (stream_queue == -1){
		perror("msgget failed");
	    exit(1);
	}

	key_t sem_key = ftok("user.c", 'S');
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}

  // Initialize semaphore to 2 (maximum number of users)
  	union semun{
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	} arg;
  
  	arg.val = MAX_USERS;
	  
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl failed");
   		exit(1);
	}
	
	while (1){
		
	    struct msgbuf msg;
	    int len = msgrcv(stream_queue, &msg, MAX_MSG_SIZE, 0, 0);
	    if (len == -1){
	    	perror("msgrcv failed");
	    	exit(1);
	    }
	
	    // Wait for semaphore
	    struct sembuf sb = {0, -1, 0};
	    if (semop(sem_id, &sb, 1) == -1)
	    {
			perror("semop failed");
			exit(1);
	    }
	    
	    // Parse user command
	    char *command = msg.mtext;
	    char *param = strchr(command, ' ');
	    if (param){
	    	*param = '\0';
	    	param++;
	    	while (isspace(*param)){
        		param++;
      		}
    	}

		//DECRYPT	
		if (!strcmp(command, "DECRYPT")){
			FILE *fp = fopen("song-playlist.json", "r");
			if (fp == NULL){
        		printf("Error opening file: %s\n", "song-playlist.json");
        		return 1;
      		}
      		
      		fseek(fp, 0, SEEK_END);
		    long file_size = ftell(fp);
		    fseek(fp, 0, SEEK_SET);
		    
      		char *json_str = malloc(file_size + 1);
		    if (json_str == NULL){
		        printf("Error allocating memory\n");
		        return 1;
		    }
	      	fread(json_str, 1, file_size, fp);
	      	fclose(fp);
	      	json_str[file_size] = '\0';

	      	cJSON *root = cJSON_Parse(json_str);
	      	if (root == NULL){
	        	printf("Error parsing JSON: %s\n", cJSON_GetErrorPtr());
	        	free(json_str);
	        	return 1;
	      	}

      		FILE *playlist = fopen("playlist.txt", "w");
      		if (playlist == NULL){
		        printf("Error opening playlist file for writing\n");
		        cJSON_Delete(root);
		        free(json_str);
		        return 1;
      		}

		    cJSON *item = NULL;
		    cJSON_ArrayForEach(item, root){
		        cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
		        cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");
				
        		if (strcmp(method->valuestring, "base64") == 0){
			        char *Dcd = base64(song->valuestring);
			        if (Dcd == NULL){
			        	printf("Error decoding base64: %s\n", song->valuestring);
			            continue;
			        }
			        printf("Base64: %s\n", Dcd);
			        fprintf(playlist, "%s\n", Dcd);
			        free(Dcd);
		        }
		        
		        else if (strcmp(method->valuestring, "hex") == 0){
		        	char *Dcd = hex(song->valuestring);
		        	if (Dcd == NULL){
		            	printf("Error decoding hex: %s\n", song->valuestring);
		            	continue;
		          	}
		        	printf("Hex: %s\n", Dcd);
		        	fprintf(playlist, "%s\n", Dcd);
		        	free(Dcd);
		        }
		        
        		else if (strcmp(method->valuestring, "rot13") == 0){
			        char *Dcd = rot13(song->valuestring);
			        if (Dcd == NULL){
			            printf("Error decoding rot13: %s\n", song->valuestring);
			            continue;
			        }
			        printf("Rot13: %s\n", Dcd);
			        fprintf(playlist, "%s\n", Dcd);
			        free(Dcd);
		        }
		        
        		else{
		        	printf("Unknown method: %s\n", method->valuestring);
		        	continue;
		        }
      		}

	    	fclose(playlist);
	    	cJSON_Delete(root);
	    	free(json_str);
    	}
    	
    	//LIST
		else if (!strcmp(command, "LIST")){
			FILE *pl = fopen("playlist.txt", "r");
			if (pl == NULL){
				printf("Error opening playlist file for reading\n");
				exit(1);
			}
			 fclose(pl);
			 system("sort playlist.txt");
  		}

		//PLAY
		else if (!strcmp(command, "PLAY")){
			
			char *query = param;
		    int num_matches = 0;
		    char matches[1000][100];
			char songnm[100];
			
		    FILE *pl = fopen("playlist.txt", "r");
		    
		    if (pl == NULL){
		        printf("Error opening playlist file for reading\n");
		        exit(1);
		    }

		    while (fgets(songnm, 100, pl) != NULL)
		    {
		        // Remove trailing newline if present
		        songnm[strcspn(songnm, "\n")] = '\0';
		
		        // Check if song name contains the query
		        char *found = strcasestr(songnm, query);
		        if (found != NULL)
		        {
		            strcpy(matches[num_matches], songnm);
		            num_matches+=1;
		        }
		        
		    }
		
		    fclose(pl);
		
		    if (num_matches == 0){
		        printf("THERE IS NO SONG CONTAINING \"%s\"\n", query);
		    }
		    
		    else if (num_matches == 1){
		         printf("USER <%d> PLAYING \"%s\"\n", getpid(), matches[0]);
		    }
		    
		    else{
		        printf("THERE ARE \"%d\" SONG(S) CONTAINING \"%s\":\n", num_matches, query);
		        for (int i = 0; i < num_matches; i++){
		            printf("%d. %s\n", i + 1, matches[i]);
		        }
		    }
  		}
  		
  		//ADD
		else if (!strcmp(command, "ADD")){
			add_song(param);
		}

	}	

	return 0;
}

```

**user.c**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/sem.h>

#define MAX_MSG_SIZE 100

//gcc -o user user.c -lpthread

struct msgbuf {
    long mtype;
    char mtext[MAX_MSG_SIZE];
};

int main() {
	
	// Create message queue and semaphore
	key_t stream_key = ftok("stream.c", 'M');
	if(stream_key == -1){
		perror("ftok failed");
	    exit(1);	
	}
	
	int stream_queue = msgget(stream_key, IPC_CREAT | 0666);
	if (stream_queue == -1){
		perror("msgget failed");
	    exit(1);
	}
	
	key_t sem_key = ftok("user.c", 'S');
	if(sem_key == -1){
		perror("ftok failed");
	    exit(1);	
	}
	
	int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
	if (sem_id == -1){
		perror("semget failed");
	    exit(1);
	}
	
	union semun {
		int val;
		struct semid_ds *buf;
		unsigned short *array;
	};
	

	union semun arg;
	struct sembuf sb = {0, -1, 0}; // Wait operation
	
	arg.val = 2;
	
	if (semctl(sem_id, 0, SETVAL, arg) == -1){
    	perror("semctl");
    	exit(1);
	}

	// Send messages to stream.c
  	char message[MAX_MSG_SIZE];
  	
  	pid_t p = getpid();
	printf("User %d connected to stream.\n", p);

  	
	while (1) {
	    // Wait for semaphore to become available
	    struct sembuf sem_wait = {0, -1, 0};
	    semop(sem_id, &sem_wait, 1);

	    printf("Enter message: ");
	    fgets(message, MAX_MSG_SIZE, stdin);
	    message[strcspn(message, "\n")] = '\0';

	    // Send message to stream.c
	    struct msgbuf msg;
	    msg.mtype = p;
	    
	    if(!strcmp(message, "DECRYPT")){
	    	strncpy(msg.mtext, message, MAX_MSG_SIZE);
		}
	    
    	else if(!strcmp(message, "LIST")){
	    	strncpy(msg.mtext, message, MAX_MSG_SIZE);
		}
		
		else if(!strncmp(message, "PLAY", 4)){
			
			char *as = strtok(message + 5, "\"");
		    if (as == NULL){
		        printf("Invalid Parameter: %s\n", message);
		        continue;
		    }
		    // Convert song to uppercase for case-insensitive comparison
		    for (int i = 0; i < strlen(as); i++){
		        as[i] = toupper(as[i]);
		    }

	    	sprintf(msg.mtext, "PLAY %s", as);
		}
		
		else if(!strncmp(message, "ADD", 3)){
			
			char *as = strtok(message + 4, "\n");
		    if (as == NULL){
		        printf("Invalid Parameter: %s\n", message);
		        continue;
		    }
	    	sprintf(msg.mtext, "ADD %s", as);
		}
		
		else{
			printf("UNKNOWN COMMAND\n");
		    continue;
		}
    
   		if(msgsnd(stream_queue, &msg, MAX_MSG_SIZE, 0) == -1){
   			perror("msgsnd failed");
	    	exit(1);	
		}

	    // Release semaphore
	    struct sembuf sem_signal = {0, 1, 0};
	    semop(sem_id, &sem_signal, 1);
  }

  // Remove message queue and semaphore
  msgctl(stream_queue, IPC_RMID, NULL);
  semctl(sem_id, 0, IPC_RMID, arg);

  return 0;
}
```

**Kesulitan Soal Nomor 3:**
1. Kebingungan untuk mengimplementasikan metode message queue.
2. Kebingungan untuk mengimplementasikan semaphore untuk melimit user.
3. Kesulitan untuk mengimplementasikan metode dekripsi.
4. Tidak familiar dengan sintaks sintaks baru.
5. Intinya semuanya kebingungan.

| <p align="center"> Solusi </p> | <p align="center"> Bukti </p> |
| -------------------------------------------- | -------------------------------------------- |
| Saya 100% dibantu oleh AI. | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/3_chatgpt.png" width = "400"/> |


**Output**
| <p align="center"> Perintah </p> | <p align="center"> Hasil Output </p> |
| -------------------------------------------- | -------------------------------------------- |
| DECRYPT | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/3_decrypt.png" width = "400"/> |
| LIST | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/3_list.png" width = "400"/> |
| PLAY | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/3_play.png" width = "400"/> |
| ADD | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/3_add.png" width = "400"/> |
| UNKNOWN COMMAND | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-3-2023-am-b12/-/raw/main/images/3_unknown.png" width = "400"/> |

## Nomor 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

a. Download dan unzip file tersebut dalam kode c bernama unzip.c.

b. Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

c. Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file

d. Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

e. Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
Path dimulai dari folder files atau categorized
Simpan di dalam log.txt
ACCESSED merupakan folder files beserta dalamnya
Urutan log tidak harus sama

f. Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.



**Penyelesaian :**

**unzip**
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
  // Download hehe.zip
  pid_t download_pid = fork();
  if (download_pid == 0) {
    execl("/usr/bin/wget", "wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL);
  } else {
    int status;
    waitpid(download_pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Download failed.\n");
      exit(1);
    }
  }

  // Unzip file
  pid_t unzip_pid = fork();
  if (unzip_pid == 0) {
    execl("/usr/bin/unzip", "unzip", "hehe.zip", NULL);
  } else {
    int status;
    waitpid(unzip_pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Unzip process failed.\n");
      exit(1);
    }
  }
  return 0;
}
```
**categorize**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475

pthread_mutex_t log_lock;
FILE *log_file;

typedef struct
{
    char dirname[MAX_LEN];
    int maxFiles;
} ThreadData;

// Fungsi untuk menulis log tentang pembuatan direktori
void logDirectoryCreation(const char *createdDir)
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);

    char logMessage[256] = "MADE ";
    char timestamp[80];
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMessage, createdDir);

    pthread_mutex_lock(&log_lock);
    fprintf(log_file, "%s %s\n", timestamp, logMessage);
    pthread_mutex_unlock(&log_lock);
}

// Fungsi untuk menulis log tentang akses direktori
void logDirectoryAccess(const char *accessedDir)
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);

    char logMessage[256] = "ACCESSED ";
    char timestamp[80];
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMessage, accessedDir);

    pthread_mutex_lock(&log_lock);
    fprintf(log_file, "%s %s\n", timestamp, logMessage);
    pthread_mutex_unlock(&log_lock);
}

// Fungsi untuk menulis log tentang pemindahan file
void *createAndOrganizeDirectories(void *arg)
{
    ThreadData *data = (ThreadData *)arg;
    char *dirname = data->dirname;
    int maxFiles = data->maxFiles;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    // hitung banyak file
    char countFile[256];
    char hehe_dir[20] = "files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE *fp;
    fp = popen(cmd, "r");
    if (fp == NULL)
    {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    logDirectoryAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);

    // menghitung total direktori yang dibuat per ext
    int numDir;
    if (maxFiles == 0)
    {
        numDir = 1;
    }
    else
    {
        numDir = numFiles / maxFiles;
        if (numFiles % maxFiles > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    // create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0)
    {
        // directory creation was successful, so do something
        logDirectoryCreation(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
        // mendefinisikan variable nama directory dengan suffix sesuai dengan index
        char _dirName[30];
        strcpy(_dirName, new_dirname);
        if (i > 0)
        {
            char suffix[10];
            sprintf(suffix, " (%d)", i + 1);
            strcat(_dirName, suffix);
        }

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0)
        {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        }
        else
        {
            // membuat direktori jika belum ada
            if (mkdir(_dirName, 0777) == 0)
            {
                logDirectoryCreation(_dirName);
                // move files according to their extensions
                char command1[600];
                char command2[500];
                char command3[100];
                char command4[600];
                char logAccessSource[200];
                char logAccessTarget[100];

                sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);

                system(logAccessSource);
                system(logAccessTarget);

                sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                if (maxFiles == 0)
                {
                    sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                    system(command4);
                }
                else
                {
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFiles, command2);

                    system(command1);
                }
            }
            else
            {
                perror("Gagal membuat direktori");
            }
        }
    }
    return NULL;
}

int main()
{
    // membuka file log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL)
    {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&log_lock, NULL) != 0)
    {
        perror("Failed to initialize log lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    ThreadData thread_args[MAX_LEN];
    int i = 0;

    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0)
    {
        mkdir("categorized", 0777);
        logDirectoryCreation("categorized");
    }

    // membaca file max.txt dan menyimpan nilainya
    char maxfilename[50] = "max.txt";
    // open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL)
    {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

    // menutup file max.txt
    fclose(max);

    // membuka file extensions.txt untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file extensions.txt berhasil dibuka
    if (fp == NULL)
    {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    // membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp))
    {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1]))
        {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFiles = maxFile;
        pthread_create(&tid[i], NULL, createAndOrganizeDirectories, &thread_args[i]);
        i++;
    }

    // join thread yang masih berjalan
    for (int j = 0; j < i; j++)
    {
        pthread_join(tid[j], NULL);
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0)
    {
        logDirectoryCreation("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);

    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    // output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir(otherPath)) != NULL)
    {
        logDirectoryAccess(otherPath);
        while ((ent = readdir(dir)) != NULL)
        {
            if (ent->d_type == DT_REG)
            {
                count++;
            }
        }
        closedir(dir);
    }
    else
    {
        perror("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    // menutup file
    fclose(fp);

    // menghancurkan mutex lock
    pthread_mutex_destroy(&log_lock);

    // menutup file log
    fclose(log_file);

    return 0;
}

```
**logchecker**
```c
// Include library yang dibutuhkan
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Struct untuk menyimpan informasi folder
typedef struct folder_stat
{
    char folder_name[256]; // Nama folder
    int file_count;        // Banyaknya file yang dikumpulkan di folder
} folder_stat;

// Struct untuk menyimpan informasi extension file
typedef struct extension_stat
{
    char ext_name[10]; // Nama extension
    int file_count;    // Banyaknya file dengan extension tersebut
} extension_stat;

// Fungsi untuk membandingkan struct folder_stat berdasarkan nama folder
int folder_stat_cmp(const void *a, const void *b)
{
    const folder_stat *stat_a = (const folder_stat *)a;
    const folder_stat *stat_b = (const folder_stat *)b;
    return strcmp(stat_a->folder_name, stat_b->folder_name);
}

// Fungsi untuk membandingkan struct extension_stat berdasarkan nama extension
int extension_stat_cmp(const void *a, const void *b)
{
    const extension_stat *stat_a = (const extension_stat *)a;
    const extension_stat *stat_b = (const extension_stat *)b;
    return strcmp(stat_a->ext_name, stat_b->ext_name);
}

int main()
{
    // Buka file log.txt
    FILE *log_file = fopen("log.txt", "r");

    // Jika gagal membuka file, tampilkan pesan error dan keluar program
    if (log_file == NULL)
    {
        perror("Error opening log file");
        exit(EXIT_FAILURE);
    }

    // Deklarasi variabel yang dibutuhkan
    char line[512];                // Buffer untuk membaca satu baris dari file
    int accessed_count = 0;        // Banyaknya ACCESSED yang dilakukan
    folder_stat folder_stats[256]; // Array untuk menyimpan informasi folder
    int folder_stat_count = 0;     // Banyaknya folder yang telah dibuat
    extension_stat ext_stats[256]; // Array untuk menyimpan informasi extension file
    int ext_stat_count = 0;        // Banyaknya extension file yang telah ditemukan

    // Loop untuk membaca file baris per baris
    while (fgets(line, sizeof(line), log_file))
    {
        // Variabel untuk menyimpan event dan detail dari baris
        char event[32], detail[512];

        // Mem-parse baris untuk mendapatkan event dan detail
        sscanf(line, "%*s %*s %s %[^\n]", event, detail);

        // Jika event adalah ACCESSED, tambahkan count-nya
        if (strcmp(event, "ACCESSED") == 0)
        {
            accessed_count++;
        }
        // Jika event adalah MADE, tambahkan informasi folder baru ke dalam array
        else if (strcmp(event, "MADE") == 0)
        {
            strcpy(folder_stats[folder_stat_count].folder_name, detail);
            folder_stats[folder_stat_count].file_count = 0;
            folder_stat_count++;
        }
        // Jika event adalah MOVED, update informasi folder dan extension file
        else if (strcmp(event, "MOVED") == 0)
        {
            // Variabel untuk menyimpan nama folder dan extension file
            char folder[256], ext[10];

            // Mem-parse detail untuk mendapatkan nama folder dan extension file
            sscanf(detail, "%s file : %*s > %s", ext, folder);

            // Loop untuk mencari index folder yang sesuai
            for (int i = 0; i < folder_stat_count; i++)
            {
                if (strcmp(folder, folder_stats[i].folder_name) == 0)
                {
                    folder_stats[i].file_count++;
                    break;
                }
            }

            int ext_idx = -1;
            for (int i = 0; i < ext_stat_count; i++)
            {
                if (strcmp(ext, ext_stats[i].ext_name) == 0)
                {
                    ext_idx = i;
                    break;
                }
            }

            if (ext_idx == -1)
            {
                strcpy(ext_stats[ext_stat_count].ext_name, ext);
                ext_stats[ext_stat_count].file_count = 1;
                ext_stat_count++;
            }
            else
            {
                ext_stats[ext_idx].file_count++;
            }
        }
    }

    fclose(log_file);

    // Sort array ext_stats berdasarkan nama extension
    qsort(ext_stats, ext_stat_count, sizeof(extension_stat), extension_stat_cmp);

    printf("Accessed count: %d\n\n", accessed_count);

    // Loop untuk menampilkan informasi folder
    printf("Folder list:\n");
    system("grep 'MOVED\\|MADE' log.txt \
            | awk -F 'categorized' '{print \"categorized\"$NF}' \
            | sed '/^$/d' \
            | sort \
            | uniq -c \
            | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");

    // Loop untuk menampilkan informasi extension file
    printf("\nExtension list:\n");

    for (int i = 0; i < ext_stat_count; i++)
    {
        printf("%s: %d files\n", ext_stats[i].ext_name, ext_stats[i].file_count);
    }

    return 0;
}

```

**Penjelasan :**

**unzip**
    #include <stdio.h>, #include <stdlib.h>, #include <unistd.h>, #include <sys/types.h>, #include <sys/wait.h>: Baris-baris ini adalah direktif preprosesor yang memasukkan file header standar yang diperlukan untuk program ini.

    int main(): Ini adalah fungsi utama dari program.

    pid_t download_pid = fork();: Pada baris ini, program melakukan fork untuk membuat proses anak yang baru. Nilai dari variabel download_pid akan berbeda antara proses anak dan proses induk.

    if (download_pid == 0): Ini adalah kondisi yang diperiksa oleh proses anak. Jika nilai download_pid adalah 0, berarti ini adalah proses anak.

    execl("/usr/bin/wget", "wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL);: Pada baris ini, fungsi execl digunakan untuk menjalankan perintah wget dengan argumen yang diberikan. Ini akan mengunduh file zip dari URL tertentu dan menyimpannya sebagai "hehe.zip".

    else: Jika nilai download_pid bukan 0, ini berarti ini adalah proses induk.

    int status; waitpid(download_pid, &status, 0);: Pada baris ini, proses induk menunggu proses anak (proses unduhan) selesai dengan menggunakan fungsi waitpid. Status keluaran dari proses anak akan disimpan dalam variabel status.

    if (WEXITSTATUS(status) != 0): Ini memeriksa status keluaran dari proses anak. Jika status keluaran bukan 0, berarti proses unduhan gagal.

    printf("Download failed.\n");: Pesan ini dicetak jika proses unduhan gagal.

    execl("/usr/bin/unzip", "unzip", "hehe.zip", NULL);: Pada baris ini, proses induk (setelah menyelesaikan proses unduhan) melakukan fork lagi untuk membuat proses anak baru yang akan menjalankan perintah unzip. Ini akan mengekstrak file zip yang telah diunduh sebelumnya.

    else: Jika nilai unzip_pid bukan 0, ini berarti ini adalah proses induk.

    int status; waitpid(unzip_pid, &status, 0);: Pada baris ini, proses induk menunggu proses anak (proses ekstraksi) selesai dengan menggunakan fungsi waitpid. Status keluaran dari proses anak akan disimpan dalam variabel status.

    if (WEXITSTATUS(status) != 0): Ini memeriksa status keluaran dari proses anak. Jika status keluaran bukan 0

**categorize**
    #include dan direktif preprosesor: Baris-baris ini memasukkan file header standar yang diperlukan untuk program ini.

    #define dan konstanta: Baris ini mendefinisikan beberapa konstanta yang digunakan dalam program, seperti panjang maksimum string, panjang maksimum jalur, maksimum ekstensi, dan ukuran maksimum tujuan.

    pthread_mutex_t dan variabel log_lock: Ini mendefinisikan mutex lock yang digunakan untuk mengamankan akses ke file log.

    FILE *log_file: Ini mendefinisikan variabel file pointer yang akan digunakan untuk membuka dan menulis file log.

    typedef struct: Ini mendefinisikan struktur data ThreadData yang digunakan untuk menyimpan data yang diperlukan oleh setiap thread.

    Fungsi logDirectoryCreation(): Ini adalah fungsi untuk menulis log tentang pembuatan direktori baru.

    Fungsi logDirectoryAccess(): Ini adalah fungsi untuk menulis log tentang akses ke direktori.

    Fungsi createAndOrganizeDirectories(): Ini adalah fungsi yang akan dijalankan oleh setiap thread. Ini menerima argumen ThreadData yang berisi nama direktori dan jumlah maksimum file yang diizinkan. Fungsi ini bertanggung jawab untuk membuat direktori baru, mengkategorikan file berdasarkan ekstensi, dan menulis log yang sesuai.

    Fungsi main(): Ini adalah fungsi utama dari program.

    a. Membuka file log.txt dan melakukan pengecekan kesalahan.

    b. Menginisialisasi mutex lock.

    c. Variabel dan pointer yang diperlukan untuk membaca file ekstensi dan file max.

    d. Membuat direktori "categorized" jika belum ada.

    e. Membaca file max.txt untuk mendapatkan jumlah maksimum file.

    f. Membuka file extensions.txt dan membaca nama direktori satu per satu.

    g. Membuat thread baru untuk setiap direktori yang dibaca dari file.

    h. Menunggu semua thread selesai.

    i. Membuat direktori "other" di dalam "categorized" jika belum ada.

    j. Memindahkan file-file lain ke direktori "categorized/other".

    k. Mengurutkan dan menampilkan isi file buffer.txt yang berisi jumlah file tiap ekstensi.

    l. Menghitung jumlah file di direktori "categorized/other" dan mencetak hasilnya.

    m. Menutup file dan membersihkan mutex lock.

    n. Menutup file log.

    o. Mengembalikan nilai 0 (sukses).

Dalam program ini, terdapat penggunaan thread untuk melakukan tugas secara paralel, serta penggunaan mutex lock untuk memastikan bahwa akses ke file log dilakukan dengan aman oleh setiap thread. Program ini juga melakukan operasi sistem seperti membuat direktori, memindahkan file, dan mengeksekusi perintah shell melalui fungsi system().

**logchecker**
    #include dan direktif preprosesor: Baris-baris ini memasukkan file header standar yang diperlukan untuk program ini.

    typedef struct: Ini mendefinisikan struktur data folder_stat dan extension_stat yang digunakan untuk menyimpan informasi tentang folder dan ekstensi file.

    Fungsi folder_stat_cmp(): Ini adalah fungsi pembanding yang digunakan dalam pengurutan array folder_stat berdasarkan nama folder.

    Fungsi extension_stat_cmp(): Ini adalah fungsi pembanding yang digunakan dalam pengurutan array extension_stat berdasarkan nama ekstensi.

    Fungsi main(): Ini adalah fungsi utama dari program.

    a. Membuka file log.txt untuk dibaca.

    b. Mengecek apakah file log.txt berhasil dibuka. Jika gagal, menampilkan pesan kesalahan dan keluar dari program.

    c. Deklarasi variabel yang diperlukan untuk menyimpan informasi yang akan dikumpulkan.

    d. Melakukan loop untuk membaca file baris per baris.

    e. Mem-parse baris untuk mendapatkan jenis event (ACCESSED, MADE, MOVED) dan detailnya.

    f. Jika event adalah ACCESSED, meningkatkan hitungan accessed_count.

    g. Jika event adalah MADE, menambahkan informasi folder baru ke dalam array folder_stats.

    h. Jika event adalah MOVED, memperbarui informasi folder dan ekstensi file yang terlibat.

    i. Menutup file log.txt setelah selesai membaca.

    j. Mengurutkan array ext_stats berdasarkan nama ekstensi.

    k. Mencetak hasil statistik, termasuk hitungan akses, daftar folder, dan daftar ekstensi file.

    l. Mengembalikan nilai 0 (sukses).

Program ini membaca file log.txt, mem-parsing setiap baris untuk mendapatkan event dan detailnya, dan mengumpulkan statistik berdasarkan event-event tersebut. Hasil statistik kemudian dicetak ke layar.

**Output :**

4A
![4A.png](./images/4A.jpeg)

4B
![4B_1_.png](./images/4B_1_.jpeg)
![4B_2_.png](./images/4B_2_.jpeg)
![4B_3_.png](./images/4B_3_.jpeg)

4C
![4C.png](./images/4C.jpeg)

4D
![4D.png](./images/4D.jpeg)

4E
![4E.png](./images/4E.jpeg)

4F
![4F.png](./images/4F.jpeg)

