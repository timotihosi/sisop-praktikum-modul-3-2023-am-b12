#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define ROW1 4
#define COL2 5

int kali(int num, int res[], int ressz){
    int carry = 0;
    for(int i=0; i<ressz; i++) {    	
        int produk = (res[i]*num) + carry;
        res[i] = produk%10;            
        carry = produk/10;     
    }
    while(carry) {                      
    	res[ressz] = carry%10;
    	carry/=10;
	ressz++;
    }
    return ressz;
}

void *fakt(void *args){
    int res[100];           
    int n;
    
    n = *((int *)args);
    res[0] = 1;
    int ressz = 1;
    for(int num = 2; num <= n; num++){
        ressz = kali(num, res, ressz);
    }
    for(int i = ressz - 1; i >= 0; i--){
        printf("%d", res[i]);
    }
    return NULL;
}


int main() {
    // stamp the start time to count execution time
    clock_t mulai, selesai;
    double waktu;
    mulai = clock();

    int shmid;
    key_t key = 1234;
    int *matrix3;

    // get shared memory segment for matrix3
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segment to matrix3
    matrix3 = shmat(shmid, NULL, 0);

    // display result
    printf("Matrix 1 x Matrix 2:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j + 18]);
        }
        printf("\n");
    }
    printf("\n");

    // calculate factorial and print the matrix
    printf("Matrix 1 x Matrix 2 (dengan faktorial):\n");
    pthread_t tid[ROW1][COL2];
    for(int i=0; i<ROW1; i++){
        for(int j=0; j<COL2; j++){
            pthread_create(&tid[i][j], NULL, fakt, &matrix3[i * COL2 + j + 18]);
            pthread_join(tid[i][j], NULL);
            printf(" ");
        }
        printf("\n");
    }
    printf("\n");

    // detach shared memory
    shmdt(matrix3);
    
    // stamp the end time and count the execution time
    selesai = clock();
    waktu = ((double) (selesai - mulai)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);

    return 0;
}
