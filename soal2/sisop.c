#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ROW1 4
#define COL2 5

int factorial(int n) {
    int j = 2; 
    int a[1000] = {0}; 
    a[0] = 1;  
    int len = 1; 
    int c = 0, num = 0;  
    while(j <= n)  
    {  
        c = 0;  
        num = 0;  
        while(c < len)  
        {  
            a[c] = a[c] * j;  
            a[c] = a[c] + num;  
            num = a[c] / 10;  
            a[c] = a[c] % 10;  
            c++;  
        }  
        while(num != 0)  
        {  
            a[len] = num % 10;  
            num = num / 10;  
            len++;  
        }  
        j++;  
    }  
    len--;  

    while(len >= 0)  
    {  
        printf("%d", a[len]);  
        len--;  
    }
    printf(" ");
}

int main() {
    // stamp the start time to count execution time
    clock_t mulai, selesai;
    double waktu;
    mulai = clock();

    int shmid;
    key_t key = 1234;
    int *matrix3;

    // get shared memory segment for matrix3
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segment to matrix3
    matrix3 = shmat(shmid, NULL, 0);

    // display result without factorial
    printf("Matrix 1 x Matrix 2 (tanpa faktorial):\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j + 18]);
        }
        printf("\n");
    }
    printf("\n");

    // calculate factorial and print the matrix
    printf("Matrix 1 x Matrix 2 (dengan faktorial):\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            factorial(matrix3[i * COL2 + j + 18]);
        }
        printf("\n");
    }

    // detach shared memory
    shmdt(matrix3);
    
    // stamp the end time and count execution time
    selesai = clock();
    waktu = ((double) (selesai - mulai)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);

    return 0;
}
