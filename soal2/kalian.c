#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int main() {
    int shmid;
    key_t key = 1234;
    int *matrix1, *matrix2, *matrix3;
    srand(time(NULL));

    // allocate shared memory for matrices
    shmid = shmget(key, (ROW1 * COL2) * sizeof(int), IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // attach shared memory segments to matrices
    matrix1 = shmat(shmid, NULL, 0);
    matrix2 = matrix1 + ROW1 * COL1;
    matrix3 = matrix2 + ROW2 * COL2;

    // initialize matrix1 with random values from 1 to 5
    for (int i = 0; i < ROW1 * COL1; i++) {
        matrix1[i] = rand() % 5 + 1;
    }

    // initialize matrix2 with random values from 1 to 4
    for (int i = 0; i < ROW2 * COL2; i++) {
        matrix2[i] = rand() % 4 + 1;
    }

    // calculate matrix multiplication
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            matrix3[i * COL2 + j] = 0;
            for (int k = 0; k < COL1; k++) {
                matrix3[i * COL2 + j] += matrix1[i * COL1 + k] * matrix2[k * COL2 + j];
            }
        }
    }

    // display result
    printf("Matrix 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            printf("%d ", matrix1[i * COL1 + j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("Matrix 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix2[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("Matrix 1 x Matrix 2:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix3[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\n");

    sleep(10);
    // detach and remove shared memory
    shmdt(matrix3);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}