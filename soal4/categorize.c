#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475

pthread_mutex_t log_lock;
FILE *log_file;

typedef struct
{
    char dirname[MAX_LEN];
    int maxFiles;
} ThreadData;

// Fungsi untuk menulis log tentang pembuatan direktori
void logDirectoryCreation(const char *createdDir)
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);

    char logMessage[256] = "MADE ";
    char timestamp[80];
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMessage, createdDir);

    pthread_mutex_lock(&log_lock);
    fprintf(log_file, "%s %s\n", timestamp, logMessage);
    pthread_mutex_unlock(&log_lock);
}

// Fungsi untuk menulis log tentang akses direktori
void logDirectoryAccess(const char *accessedDir)
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);

    char logMessage[256] = "ACCESSED ";
    char timestamp[80];
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMessage, accessedDir);

    pthread_mutex_lock(&log_lock);
    fprintf(log_file, "%s %s\n", timestamp, logMessage);
    pthread_mutex_unlock(&log_lock);
}

// Fungsi untuk menulis log tentang pemindahan file
void *createAndOrganizeDirectories(void *arg)
{
    ThreadData *data = (ThreadData *)arg;
    char *dirname = data->dirname;
    int maxFiles = data->maxFiles;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    // hitung banyak file
    char countFile[256];
    char hehe_dir[20] = "files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE *fp;
    fp = popen(cmd, "r");
    if (fp == NULL)
    {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    logDirectoryAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);

    // menghitung total direktori yang dibuat per ext
    int numDir;
    if (maxFiles == 0)
    {
        numDir = 1;
    }
    else
    {
        numDir = numFiles / maxFiles;
        if (numFiles % maxFiles > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    // create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0)
    {
        // directory creation was successful, so do something
        logDirectoryCreation(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
        // mendefinisikan variable nama directory dengan suffix sesuai dengan index
        char _dirName[30];
        strcpy(_dirName, new_dirname);
        if (i > 0)
        {
            char suffix[10];
            sprintf(suffix, " (%d)", i + 1);
            strcat(_dirName, suffix);
        }

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0)
        {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        }
        else
        {
            // membuat direktori jika belum ada
            if (mkdir(_dirName, 0777) == 0)
            {
                logDirectoryCreation(_dirName);
                // move files according to their extensions
                char command1[600];
                char command2[500];
                char command3[100];
                char command4[600];
                char logAccessSource[200];
                char logAccessTarget[100];

                sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);

                system(logAccessSource);
                system(logAccessTarget);

                sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                if (maxFiles == 0)
                {
                    sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                    system(command4);
                }
                else
                {
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFiles, command2);

                    system(command1);
                }
            }
            else
            {
                perror("Gagal membuat direktori");
            }
        }
    }
    return NULL;
}

int main()
{
    // membuka file log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL)
    {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&log_lock, NULL) != 0)
    {
        perror("Failed to initialize log lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    ThreadData thread_args[MAX_LEN];
    int i = 0;

    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0)
    {
        mkdir("categorized", 0777);
        logDirectoryCreation("categorized");
    }

    // membaca file max.txt dan menyimpan nilainya
    char maxfilename[50] = "max.txt";
    // open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL)
    {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

    // menutup file max.txt
    fclose(max);

    // membuka file extensions.txt untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file extensions.txt berhasil dibuka
    if (fp == NULL)
    {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    // membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp))
    {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1]))
        {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFiles = maxFile;
        pthread_create(&tid[i], NULL, createAndOrganizeDirectories, &thread_args[i]);
        i++;
    }

    // join thread yang masih berjalan
    for (int j = 0; j < i; j++)
    {
        pthread_join(tid[j], NULL);
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0)
    {
        logDirectoryCreation("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);

    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    // output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir(otherPath)) != NULL)
    {
        logDirectoryAccess(otherPath);
        while ((ent = readdir(dir)) != NULL)
        {
            if (ent->d_type == DT_REG)
            {
                count++;
            }
        }
        closedir(dir);
    }
    else
    {
        perror("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    // menutup file
    fclose(fp);

    // menghancurkan mutex lock
    pthread_mutex_destroy(&log_lock);

    // menutup file log
    fclose(log_file);

    return 0;
}
