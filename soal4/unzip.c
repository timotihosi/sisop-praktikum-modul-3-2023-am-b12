#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
  // Download hehe.zip
  pid_t download_pid = fork();
  if (download_pid == 0) {
    execl("/usr/bin/wget", "wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL);
  } else {
    int status;
    waitpid(download_pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Download failed.\n");
      exit(1);
    }
  }

  // Unzip file
  pid_t unzip_pid = fork();
  if (unzip_pid == 0) {
    execl("/usr/bin/unzip", "unzip", "hehe.zip", NULL);
  } else {
    int status;
    waitpid(unzip_pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Unzip process failed.\n");
      exit(1);
    }
  }
  return 0;
}
