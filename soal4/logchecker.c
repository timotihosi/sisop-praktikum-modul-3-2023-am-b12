// Include library yang dibutuhkan
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Struct untuk menyimpan informasi folder
typedef struct folder_stat
{
    char folder_name[256]; // Nama folder
    int file_count;        // Banyaknya file yang dikumpulkan di folder
} folder_stat;

// Struct untuk menyimpan informasi extension file
typedef struct extension_stat
{
    char ext_name[10]; // Nama extension
    int file_count;    // Banyaknya file dengan extension tersebut
} extension_stat;

// Fungsi untuk membandingkan struct folder_stat berdasarkan nama folder
int folder_stat_cmp(const void *a, const void *b)
{
    const folder_stat *stat_a = (const folder_stat *)a;
    const folder_stat *stat_b = (const folder_stat *)b;
    return strcmp(stat_a->folder_name, stat_b->folder_name);
}

// Fungsi untuk membandingkan struct extension_stat berdasarkan nama extension
int extension_stat_cmp(const void *a, const void *b)
{
    const extension_stat *stat_a = (const extension_stat *)a;
    const extension_stat *stat_b = (const extension_stat *)b;
    return strcmp(stat_a->ext_name, stat_b->ext_name);
}

int main()
{
    // Buka file log.txt
    FILE *log_file = fopen("log.txt", "r");

    // Jika gagal membuka file, tampilkan pesan error dan keluar program
    if (log_file == NULL)
    {
        perror("Error opening log file");
        exit(EXIT_FAILURE);
    }

    // Deklarasi variabel yang dibutuhkan
    char line[512];                // Buffer untuk membaca satu baris dari file
    int accessed_count = 0;        // Banyaknya ACCESSED yang dilakukan
    folder_stat folder_stats[256]; // Array untuk menyimpan informasi folder
    int folder_stat_count = 0;     // Banyaknya folder yang telah dibuat
    extension_stat ext_stats[256]; // Array untuk menyimpan informasi extension file
    int ext_stat_count = 0;        // Banyaknya extension file yang telah ditemukan

    // Loop untuk membaca file baris per baris
    while (fgets(line, sizeof(line), log_file))
    {
        // Variabel untuk menyimpan event dan detail dari baris
        char event[32], detail[512];

        // Mem-parse baris untuk mendapatkan event dan detail
        sscanf(line, "%*s %*s %s %[^\n]", event, detail);

        // Jika event adalah ACCESSED, tambahkan count-nya
        if (strcmp(event, "ACCESSED") == 0)
        {
            accessed_count++;
        }
        // Jika event adalah MADE, tambahkan informasi folder baru ke dalam array
        else if (strcmp(event, "MADE") == 0)
        {
            strcpy(folder_stats[folder_stat_count].folder_name, detail);
            folder_stats[folder_stat_count].file_count = 0;
            folder_stat_count++;
        }
        // Jika event adalah MOVED, update informasi folder dan extension file
        else if (strcmp(event, "MOVED") == 0)
        {
            // Variabel untuk menyimpan nama folder dan extension file
            char folder[256], ext[10];

            // Mem-parse detail untuk mendapatkan nama folder dan extension file
            sscanf(detail, "%s file : %*s > %s", ext, folder);

            // Loop untuk mencari index folder yang sesuai
            for (int i = 0; i < folder_stat_count; i++)
            {
                if (strcmp(folder, folder_stats[i].folder_name) == 0)
                {
                    folder_stats[i].file_count++;
                    break;
                }
            }

            int ext_idx = -1;
            for (int i = 0; i < ext_stat_count; i++)
            {
                if (strcmp(ext, ext_stats[i].ext_name) == 0)
                {
                    ext_idx = i;
                    break;
                }
            }

            if (ext_idx == -1)
            {
                strcpy(ext_stats[ext_stat_count].ext_name, ext);
                ext_stats[ext_stat_count].file_count = 1;
                ext_stat_count++;
            }
            else
            {
                ext_stats[ext_idx].file_count++;
            }
        }
    }

    fclose(log_file);

    // Sort array ext_stats berdasarkan nama extension
    qsort(ext_stats, ext_stat_count, sizeof(extension_stat), extension_stat_cmp);

    printf("Accessed count: %d\n\n", accessed_count);

    // Loop untuk menampilkan informasi folder
    printf("Folder list:\n");
    system("grep 'MOVED\\|MADE' log.txt \
            | awk -F 'categorized' '{print \"categorized\"$NF}' \
            | sed '/^$/d' \
            | sort \
            | uniq -c \
            | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");

    // Loop untuk menampilkan informasi extension file
    printf("\nExtension list:\n");

    for (int i = 0; i < ext_stat_count; i++)
    {
        printf("%s: %d files\n", ext_stats[i].ext_name, ext_stats[i].file_count);
    }

    return 0;
}
